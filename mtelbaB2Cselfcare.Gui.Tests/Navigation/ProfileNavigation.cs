﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Navigation
{
    class ProfileNavigation : PagesInner
    {
        public ProfileNavigation(IWebDriver driver) : base(driver)
        { }

        public string[,] ProfileMenuItems = { 
            { "Uvezani nalozi", PageUrls.AccountsUrl}, 
            { "Promjena e-mail-a mog m:tel naloga", PageUrls .ProfileChangeEmailUrl}, 
            { "Promjena lozinke", PageUrls.ProfileChangePasswordUrl},
            { "Brisanje mog m:tel naloga", PageUrls.ProfileDeleteAccountUrl} };

        [FindsBy(How = How.XPath, Using = "//*[@class='sub-nav-menu__desktop-item-inner']")]
        private IList<IWebElement> MenuItems;

        [FindsBy(How = How.XPath, Using = "//*[@class='header-info__profile-title']")]
        protected IWebElement PageHeaderTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='header-info__profile-email']")]
        protected IWebElement PageHeaderEmail;

        public string GetHeaderTitle()
        {
            return TestsHelper.GetWebText(PageHeaderTitle);
        }

        public string GetHeaderEmail()
        {
            return TestsHelper.GetWebText(PageHeaderEmail);
        }

        public int ProfileCountShouldBe()
        {
            return ProfileMenuItems.GetLength(0);
        }

        public int GetProfileCount()
        {
            return MenuItems.Count;
        }

        public string ProfileMenuItemShouldBe(int index)
        {
            return ProfileMenuItems[index, 0];
        }

        public string GetProfileMenuItem(int index)
        {
            return MenuItems[index].Text;
        }

        public string ProfileMenuPageUrlShouldBe(int index)
        {
            return ProfileMenuItems[index, 1];
        }

        public void ClickMenuItem(int index)
        {
            TestsHelper.ClickWeb(MenuItems[index]);
        }
    }
}
