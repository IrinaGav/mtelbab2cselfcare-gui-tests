﻿using System;

namespace mtelbaB2Cselfcare.Gui.Tests.Helpers
{
    class AssertsHelper
    {
        private string Errors { get; set; }
        private bool AssertsPassed { get; set; }

        public AssertsHelper()
        {
            AssertsPassed = true;
            Errors = string.Empty;
        }

        private void RegisterError(string exceptionMessage)
        {
            AssertsPassed = false;
            Errors = exceptionMessage;
        }

        public void Accumulate(Action assert)
        {
            try
            {
                assert.Invoke();
            }
            catch (Exception exception)
            {
                RegisterError(exception.Message);
            }
        }

        public void Release()
        {
            if (!AssertsPassed)
            {
                throw new NUnit.Framework.AssertionException(Errors);
            }
        }
    }
}
