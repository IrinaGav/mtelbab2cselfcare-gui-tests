﻿using mtelbaB2Cselfcare.Gui.Tests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace mtelbaB2Cselfcare.Gui.Tests.Helpers
{
    class TestSettings
    {
        public static int DriverImplicitWaitSec = 2;
        public static int SleepMillisec = 500;
        public static int TimesSleep = 10;
    }

    class TestsHelper
    {
        public static void StartDriver(IWebDriver driver)
        {
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TestSettings.DriverImplicitWaitSec);
            driver.Manage().Cookies.DeleteAllCookies();
        }

        public static bool OpenPage(IWebDriver driver, string pageUrl)
        {
            driver.Url = pageUrl;
            return isPageOpened(driver, pageUrl);
        }

        public static bool isPageOpened(IWebDriver driver, string pageUrl)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TestSettings.DriverImplicitWaitSec));
            try
            {
                wait.Until(foo => Helper.isPageOpened(driver, pageUrl));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool isPageOpenedUrlStartedWith(string pageUrl, IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TestSettings.DriverImplicitWaitSec));
            try
            {
                wait.Until(foo => Helper.isPageOpenedUrlStartedWith(pageUrl, driver));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool isPageLoaded(IWebDriver driver, string pageLoader)
        {
            return Wait(() => Helper.isPageLoaded(driver, pageLoader));
        }

        public static string UserLogin_OpenPage(string user, string password, LoginPage loginPage, dynamic page, IWebDriver driver)
        {
            loginPage.loginByUser(user, password);
            if (!page.isProfileDisplayed())
            {
                return "User can't login. Test is interrupted.";
            }

            page.OpenPage(page.PageUrl);
            if (!page.isPageOpened(page.PageUrl))
            {
                return string.Format("Change password page is not opened, expected {0}, but is {1}. Test is interrupted.", page.PageUrl, driver.Url);
            }
            return string.Empty;
        }

        public static bool isMandatoryInput(IWebElement web, string inputValue, IWebElement webErrorMsg)
        {
            Helper.EnterValue(web, inputValue);
            Helper.ClearValue(web);
            return webErrorMsg.Text.Equals(PageMessages.Msg_FieldMandatory);
        }

        public static bool isIconDisplayed(IWebElement web)
        {
            if (Wait(() => Helper.isWebExist(web)))
            {
                if (Wait(() => web.Displayed))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool isWebEnabled(IWebElement web)
        {
            return Wait(() => Helper.isWebEnabled(web));
        }

        public static bool isWebDisabled(IWebElement web)
        {
            return Wait(() => Helper.isWebDisabled(web));
        }

        public static bool isWebDisplayed(IWebElement web)
        {
            return Wait(() => Helper.isWebDisplayed(web));
        }

        public static void ClickWeb(IWebElement web)
        {
            if (isWebEnabled(web))
            {
                Helper.ClickWeb(web);
            }
        }

        public static void ClickWebAfterWebNotExist(IWebElement web, IWebElement waitWebNotExist)
        {
            while (Helper.isWebExist(waitWebNotExist))
            {
                Thread.Sleep(TestSettings.SleepMillisec);
            }
            if (isWebEnabled(web))
            {
                Helper.ClickWeb(web);
            }
        }

        public static string GetWebText(IWebElement web)
        {
            if (isWebEnabled(web))
            {
                if (Wait(() => web.Text.Length > 0))
                {
                    return web.Text;
                }
            }
            return string.Empty;
        }

        public static string WaitWebText(IWebElement web, string textValue)
        {
            if (Helper.isWebExist(web))
            {
                if (Wait(() => web.Text.Length > 0 & web.Text.Equals(textValue)))
                {
                    return web.Text;
                }
                return web.Text;
            }
            return string.Empty;
        }

        public static string WaitNotification(IWebElement web, string textValue)
        {
            try
            {
                if (isWebExist(web))
                {
                    if (Wait(() => web.Text.Length > 0))
                    {
                        return web.Text;
                    }
                    return string.Empty;
                }
                return string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static bool isWebExist(IWebElement web)
        {
            return Wait(() => Helper.isWebExist(web));
        }

        public static void EnterValue(IWebElement web, string inputValue)
        {
            if (Wait(() => isWebEnabled(web)))
            {
                if (inputValue.Length == 0)
                {
                    Helper.ClearValue(web);
                }
                else
                {
                    Helper.EnterValue(web, inputValue);
                }
                Thread.Sleep(TestSettings.SleepMillisec);
            }
        }

        public static bool Wait(Func<bool> condition)
        {
            for (int i = 0; i < TestSettings.TimesSleep; i++)
            {
                try
                {
                    if (condition())
                    {
                        return true;
                    }
                    Thread.Sleep(TestSettings.SleepMillisec);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return false;
        }
    }
}
