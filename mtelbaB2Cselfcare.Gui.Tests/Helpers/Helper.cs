﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;

namespace mtelbaB2Cselfcare.Gui.Tests.Helpers
{
    class Helper
    {
        public static string AddText(string stringText, string addText)
        {
            if (addText.Length > 0)
            {
                return stringText + (stringText.Length > 0 ? "; " : "") + addText;
            }
            return stringText;
        }

        public static int GetRandomIndex(int minValue, int maxValue)
        {
            System.Random rnd = new System.Random();
            int value = rnd.Next(minValue, maxValue);
            return value;
        }

        public static void EnterValue(IWebElement web, string inputValue)
        {
            web.SendKeys(inputValue);
        }

        public static void ClearValue(IWebElement web)
        {
            int nn = web.GetAttribute("value").Length;
            for (int i = 0; i < nn; i++)
            {
                web.SendKeys(Keys.Backspace);
            }
        }

        public static void HoverCursorOnWeb(IWebDriver driver, IWebElement web)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(web).Perform();
        }

        public static void ClickWeb(IWebElement web)
        {
            web.Click();
        }

        public static bool isWebExist(IWebElement web)
        {
            try
            {
                if (web.TagName.Length > 0)
                {
                    return true;
                }
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            return false;
        }

        public static bool isPageOpened(IWebDriver driver, string expectedPageUrl)
        {
            if (driver.Url == expectedPageUrl)
            {
                return true;
            }
            return false;
        }

        public static bool isPageOpenedUrlStartedWith(string expectedPageUrl, IWebDriver driver)
        {
            if (driver.Url.StartsWith(expectedPageUrl))
            {
                return true;
            }
            return false;
        }

        public static bool isPageLoaded(IWebDriver driver, string pageLoader)
        {
            ReadOnlyCollection<IWebElement> ss = driver.FindElements(By.XPath(pageLoader));
            if (ss.Count == 0)
            {
                return true;
            }
            if (ss[0].GetCssValue("display").Equals("none"))
            {
                return true;
            }
            return false;
        }

        public static bool isWebEnabled(IWebElement web)
        {
            return web.Enabled;
        }

        public static bool isWebDisabled(IWebElement web)
        {
            return !web.Enabled;
        }

        public static bool isWebDisplayed(IWebElement web)
        {
            return web.Displayed;
        }
    }
}
