﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Navigation;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestNavigation
{
    [TestFixture][TestFixture]
    [Category("Navigation")]
    class Navigation_ProfileMenu : TestSet
    {
        ProfileAccountsPage page;
        LoginPage loginPage;
        private string testUser;
        private string testPassword;
        private string testHeader;

        public Navigation_ProfileMenu()
        {
            testUser = SpecificData.GeneralTestUser[0, 0];
            testPassword = SpecificData.GeneralTestUser[0, 1];
            testHeader = SpecificData.GeneralTestUser[0, 2];
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            _assertsAccumulator = new AssertsHelper();
            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));

            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.AccountsUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.AccountsUrl, driver.Url));

            page = new ProfileAccountsPage(driver);
            page.isPageLoaded();
        }

        [Test]
        public void Navigation_CheckProfileItems()
        {
            page.OpenProfileViaMenu();

            ProfileNavigation profile = new ProfileNavigation(driver);

            int profileMenuItems = profile.GetProfileCount();

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(profile.ProfileCountShouldBe(), profileMenuItems, "Profile navigation failed: count of items is wrong"));

            for (int i = 0; i < profileMenuItems; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(profile.ProfileMenuItemShouldBe(i), profile.GetProfileMenuItem(i), string.Format("Profile menu item {0} failed", i)));
            }
        }

        [Test]
        public void Navigation_ShouldOpenProfilePages()
        {
            page.OpenProfileViaMenu();

            ProfileNavigation profile = new ProfileNavigation(driver);

            int profileMenuItems = profile.GetProfileCount();

            for (int i = 0; i < profileMenuItems; i++)
            {
                profile.ClickMenuItem(i);
                profile.isPageLoaded();
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(profile.ProfileMenuPageUrlShouldBe(i), driver.Url, string.Format("Profile menu item {0} failed: incorrect page is opened", i)));
            }
        }

        [Test]
        public void Navigation_CheckProfileHeader()
        {
            page.OpenProfileViaMenu();

            ProfileNavigation profile = new ProfileNavigation(driver);

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testHeader, profile.GetHeaderTitle(), "Profile navigation failed: header title is wrong"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testUser, profile.GetHeaderEmail(), "Profile navigation failed: header email is wrong"));
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
