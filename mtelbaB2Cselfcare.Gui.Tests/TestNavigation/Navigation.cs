﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestNavigation
{
    [TestFixture]
    [Category("Navigation")]
    class Navigation : TestSet
    {
        [Test]
        public void Navigation_ShouldGoFromLoginToRegistrationAndBack()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            LoginPage loginPage = new LoginPage(driver);

            loginPage.ClickLinkToRegistration();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}", PageUrls.RegistrationUrl, driver.Url));

            RegistrationPage page = new RegistrationPage(driver);

            Assert.IsTrue(page.isPageLoaded(), "Registration page is not loaded");

            page.ClickLinkToLogin();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.LoginUrl),
                string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
        }

        [Test]
        public void Navigation_ShouldGoFromLoginToForgotPasswordAndBack()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            LoginPage loginPage = new LoginPage(driver);

            loginPage.ClickLinkToForgotPassword();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.ForgottenPasswordUrl),
                string.Format("Forgotten Password page is not opened, expected {0}, but is {1}", PageUrls.ForgottenPasswordUrl, driver.Url));

            ForgottenPasswordPage page = new ForgottenPasswordPage(driver);

            Assert.IsTrue(page.isPageLoaded(), "Forgotten password page is not loaded");

            page.ClickLinkToLogin();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.LoginUrl),
                string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
        }
    }
}
