﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestFlows
{
    [TestFixture]
    class RegistrationFlow : TestSet
    {
        RegistrationPage page;

        [SetUp]
        public void SetUp()
        {
            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Category("TestFlows")]
        [Category("TestEmails")]
        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckFixedNewAccount))]
        public void RegistrationFixedServiceNewAccount_ShouldSendAndResendEmail(string subscriberCode, string referenceNumber)
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), 
                string.Format("Login page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.LoginUrl, driver.Url));
            LoginPage loginPage = new LoginPage(driver);

            loginPage.ClickLinkToRegistration();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.RegistrationUrl, driver.Url));

            RegistrationPage registrationPage = new RegistrationPage(driver);

            registrationPage.EnterSubscription(subscriberCode);
            registrationPage.EnterNumber(referenceNumber);
            Assert.IsTrue(registrationPage.isFixedServiceButtonEnabled(), "Fixed service inputs failed: button is not enabled. Test is interrupted");

            registrationPage.ClickFixedServiceButton();
            registrationPage.isPageLoaded();

            RegistrationNewAccountPage newAccountPage = new RegistrationNewAccountPage(driver);
            Assert.IsTrue(newAccountPage.isFormPageLoaded(), "Continue Registration failed: NewAccountForm is not loaded. Test is interrupted");

            var newAccountEmail = CommonData.newUserEmail;
            newAccountPage.EnterFirstName("a");
            newAccountPage.EnterLastName("b");
            newAccountPage.EnterEmail(newAccountEmail);
            newAccountPage.EnterPassword(CommonData.CommonPassword);
            newAccountPage.EnterPasswordRepeat(CommonData.CommonPassword);
            newAccountPage.ClickCheckbox();

            Assert.IsTrue(newAccountPage.isButtonEnabled(), "New Account entering data failed: button is not enabled. Test is interrupted");

            newAccountPage.ClickButton();
            newAccountPage.isPageLoaded();

            Assert.IsTrue(TestsHelper.isPageOpenedUrlStartedWith(PageUrls.RegistrationConfirmationUrl, driver), "Registration failed: Confirmation page EmailSent is not opened. Test is interrupted");

            RegistrationConfirmationPage confirmationPage = new RegistrationConfirmationPage(driver);

            confirmationPage.ClickButton();

            var expectedMsg = confirmationPage.GetNotificationMsg("EmaiSent", newAccountEmail);
            var notification = confirmationPage.WaitNotification(expectedMsg);
            Assert.IsTrue(notification.Length > 0 & notification.Equals(expectedMsg), 
                string.Format("Registration email resending failed: notification expected {0}, but is {1}. Test is interrupted", expectedMsg, notification));
        }

        [Category("TestFlows")]
        [Category("TestEmails")]
        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckMobileAccountExistMain))]
        public void RegistrationMobileExistAccount_ShouldLogin(string phoneValue, string otpCode, string email, string msg)
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.LoginUrl, driver.Url));
            LoginPage loginPage = new LoginPage(driver);

            loginPage.ClickLinkToRegistration();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.RegistrationUrl, driver.Url));

            RegistrationPage registrationPage = new RegistrationPage(driver);

            registrationPage.EnterPhone(phoneValue);
            registrationPage.ClickLinkSmsSend();
            registrationPage.isPageLoaded();

            registrationPage.EnterOtp(otpCode);
            Assert.IsTrue(registrationPage.isMobileServiceButtonEnabled(), "Mobile service inputs failed: button is not enabled. Test is interrupted");

            registrationPage.ClickMobileServiceButton();
            registrationPage.isPageLoaded();

            RegistrationExistAccountPage accountExistPage = new RegistrationExistAccountPage(driver);
            Assert.IsTrue(accountExistPage.isFormPageLoaded(), "Continue Registration failed: ExistAccountForm is not loaded. Test is interrupted");

            accountExistPage.EnterPassword(CommonData.CommonPassword);
            accountExistPage.ClickButtonLogin();
            accountExistPage.isPageLoaded();

            ProfileAccountsPage accountsPage = new ProfileAccountsPage(driver);
            Assert.IsTrue(accountExistPage.isProfileDisplayed(), "User with exist account failed: cannot login. Test is interrupted");
            accountsPage.LogOut();
        }

        [Category("TestFlows")]
        [Category("TestEmails")]
        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckMobileAccountExistMain))]
        public void RegistrationMobileExistAccount_ShouldGoToForgotPassword(string phoneValue, string otpCode, string email, string msg)
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), 
                string.Format("Login page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.LoginUrl, driver.Url));
            LoginPage loginPage = new LoginPage(driver);

            loginPage.ClickLinkToRegistration();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.RegistrationUrl, driver.Url));

            RegistrationPage registrationPage = new RegistrationPage(driver);

            registrationPage.EnterPhone(phoneValue);
            registrationPage.ClickLinkSmsSend();
            registrationPage.isPageLoaded();

            registrationPage.EnterOtp(otpCode);
            Assert.IsTrue(registrationPage.isMobileServiceButtonEnabled(), "Mobile service inputs failed: button is not enabled. Test is interrupted");

            registrationPage.ClickMobileServiceButton();
            registrationPage.isPageLoaded();

            RegistrationExistAccountPage accountExistPage = new RegistrationExistAccountPage(driver);
            Assert.IsTrue(accountExistPage.isFormPageLoaded(), "Continue Registration failed: ExistAccountForm is not loaded. Test is interrupted");

            accountExistPage.ClickLinkToForgotPassword();
            accountExistPage.isPageLoaded();

            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.ForgottenPasswordUrl),
                string.Format("Forgotten Password page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.ForgottenPasswordUrl, driver.Url));
        }
    }
}
