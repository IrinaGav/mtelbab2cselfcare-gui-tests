﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestFlows
{
    [TestFixture]
    class ForgottenPasswordFlow : TestSet
    {
        ForgottenPasswordPage page;

        [SetUp]
        public void SetUp()
        {
            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Category("TestEmails")]
        [Test]
        public void ForgottenPassword_ShouldSendAndResendEmailToUserBox()
        {
            ForgottenPassword_ShouldSendAndResendEmail(CommonData.userEmail, "");
        }

        [Category("TestFlows")]
        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckForgottenPasswordFlow))]
        public void ForgottenPassword_ShouldSendAndResendEmail(string user, string stub)
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl),
                string.Format("Login page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.LoginUrl, driver.Url));

            LoginPage loginPage = new LoginPage(driver);
            loginPage.ClickLinkToForgotPassword();

            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.ForgottenPasswordUrl),
                string.Format("Forgotten password page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.ForgottenPasswordUrl, driver.Url));

            ForgottenPasswordPage page = new ForgottenPasswordPage(driver);

            page.EnterEmail(user);
            page.ClickButton();
            page.isPageLoaded();

            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.PasswordConfirmationUrl + string.Format("#email={0}", user)),
                string.Format("Failed: Confirmation page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.PasswordConfirmationUrl, driver.Url));

            PasswordConfirmationPage confirmationPage = new PasswordConfirmationPage(driver);

            confirmationPage.ClickButton();

            var expectedMsg = confirmationPage.GetNotificationMsg("EmaiSent", user);
            var notification = confirmationPage.WaitNotification(expectedMsg);
            Assert.IsTrue(notification.Length > 0 & notification.Equals(expectedMsg), 
                string.Format("Email resending failed: notification expected '{0}', but is '{1}'. Test is interrupted", expectedMsg, notification));
        }
    }
}
