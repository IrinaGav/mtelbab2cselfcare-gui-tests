﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.TestFlows
{
    [TestFixture]
    class ProfileAccountsFlow : TestSet
    {
        ProfileAccountsPage page;
        LoginPage loginPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Category("TestPages")]
        [Test, TestCaseSource(typeof(ProfileAccountsData), nameof(ProfileAccountsData.CheckMobileExistMainData))]
        public void Accounts_CheckAddedMobileServiceIsMain(string user, string password, string mobile, string otpCode, string serviceUser, string servicePassword)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.AccountsUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.AccountsUrl, driver.Url));

            page = new ProfileAccountsPage(driver);

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.activateAddMobileServiceTab();
            addServicePage.EnterPhone(mobile);
            addServicePage.ClickLinkSmsSend();
            addServicePage.isPageLoaded();

            addServicePage.EnterOtp(otpCode);

            var buttonEnabled = addServicePage.isMobileServiceButtonEnabled();
            Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled. Test is interrupted");

            addServicePage.ClickMobileServiceButton();
            addServicePage.isPageLoaded();

            var expectedMsg = addServicePage.GetNotificationMsg("MainService");
            var waitErrorNotification = addServicePage.WaitNotification(expectedMsg);
            Assert.IsTrue(waitErrorNotification.Length > 0, "Adding main service failed: error notification is not shown");
            Assert.AreEqual(expectedMsg, waitErrorNotification, "Adding main service failed: error notification is not correct");

            addServicePage.ClickMobileCancelButton();
        }

        [Category("TestFlows")]
        [Test, TestCaseSource(typeof(ProfileAccountsData), nameof(ProfileAccountsData.CheckAddMobileService))]
        public void Accounts_ShouldAddMobileServiceThenUnlink(string user, string password, string mobile, string otpCode)
        {
            string testUserName = "Test User";
            string testUserLastName = "for linking";

            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.AccountsUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.AccountsUrl, driver.Url));

            page = new ProfileAccountsPage(driver);

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.activateAddMobileServiceTab();
            addServicePage.EnterPhone(mobile);
            addServicePage.ClickLinkSmsSend();
            addServicePage.isPageLoaded();

            addServicePage.EnterOtp(otpCode);

            var buttonEnabled = addServicePage.isMobileServiceButtonEnabled();
            Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled. Test is interrupted");

            addServicePage.ClickMobileServiceButton();
            addServicePage.isPageLoaded();

            addServicePage.EnterName("");
            addServicePage.EnterLastName("");

            addServicePage.EnterName(testUserName);
            addServicePage.EnterLastName(testUserLastName);

            addServicePage.ClickAddServiceConnectButton();
            addServicePage.isPageLoaded();

            var expectedMsg = addServicePage.GetNotificationMsg("Linked");
            var waitNotification = addServicePage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Adding main service failed: error notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Adding main service failed: error notification is not correct"));

            var pageNew = new ProfileAccountsPage(driver);
            var listServices = pageNew.GetListServices();
            int serviceLinkedResult = getServiceIndex(listServices, string.Format("{0} {1}", testUserName, testUserLastName));

            string serviceIsLinkedResultInfo = serviceLinkedResult == -1 ? "Failed: linked service is not in the services list" : "";
            serviceIsLinkedResultInfo = serviceLinkedResult == 0 ? "Failed: linked service is set as main" : serviceIsLinkedResultInfo;
            Assert.IsTrue(serviceLinkedResult > 0, serviceIsLinkedResultInfo);

            pageNew.ClickUnlink(serviceLinkedResult - 1);
            pageNew.isPageLoaded();

            var popupDisplayed = pageNew.isPopupDisplayed();
            Assert.IsTrue(popupDisplayed, "Failed: confirmation popup to unlink is not opened");

            pageNew.ClickPopupButton();
            pageNew.isPageLoaded();

            expectedMsg = addServicePage.GetNotificationMsg("Unlinked");
            waitNotification = addServicePage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Unlinking service failed: notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Unlinking service failed: notification is not correct"));

            listServices = pageNew.GetListServices();
            serviceLinkedResult = getServiceIndex(listServices, string.Format("{0} {1}", testUserName, testUserLastName));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(serviceLinkedResult ==-1, "Failed: test linked service is not unlinked"));
        }

        private int getServiceIndex(List<string> listServices, string userName)
        {
            int serviceIndex = -1;

            int i = 0;
            foreach (var item in listServices)
            {
                if (item.Equals(userName))
                {
                    serviceIndex = i;
                    break;
                }
                i++;
            }
            return serviceIndex;
        }

        [TearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
