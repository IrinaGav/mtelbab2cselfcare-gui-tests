﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    class PostpaidMobileOverview : TestSet
    {
        LoginPage loginPage;
        PostpaidMobileOverviewPage page;
        private string testUser;
        private string testPassword;
        private string testServiceId;

        public PostpaidMobileOverview()
        {
            testUser = SpecificData.PostpaidMobileResourcesUser[0, 0];
            testPassword = SpecificData.PostpaidMobileResourcesUser[0, 1];
            testServiceId = SpecificData.PostpaidMobileResourcesUser[0, 2];
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));

            string pageUrl = string.Format(PageUrls.PostpaidMobileOverviewUrl, testServiceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Overview page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PostpaidMobileOverviewPage(driver);
            page.isPageLoaded();

            _assertsAccumulator = new AssertsHelper();
        }

        [Category("TestPages")]
        [Test]
        public void PostpaidMobileResources_CheckResoucesAndBannerElements()
        {
            page.isPageLoaded();

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckResources(_assertsAccumulator, testServiceId);

            page.CheckBanner(_assertsAccumulator);
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
