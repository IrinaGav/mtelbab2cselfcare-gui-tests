﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class Login : TestSet
    {
        LoginPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            page = new LoginPage(driver);

           Assert.IsTrue(page.isPageLoaded(), "Login page is not loaded");

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Test]
        public void Login_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void Login_CheckMandatoryFields()
        {
            driver.Navigate().Refresh();
            page.CheckMandatoryInputs(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(LoginData), nameof(LoginData.CheckInvalidInputEmail))]
        public void Login_CheckInvalidInputEmail(string inputValue)
        {
            driver.Navigate().Refresh();
            page.EnterEmail(inputValue);

            var errorMsg = page.GetMsgInputEmail();

            _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Input email failed: the error message isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_InvalidEmail, errorMsg, "Wrong text for error message if email is incorrect"));
        }

        [Test, TestCaseSource(typeof(LoginData), nameof(LoginData.CheckWrongLogin))]
        public void Login_ShouldNotifyWrongData(string user, string password)
        {
            driver.Navigate().Refresh();
            page.EnterEmail(user);
            page.EnterPassword(password);
            page.ClickButton();
            page.isPageLoaded();

            var errorMsg = page.GetMsgInputEmail();

            _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "The error message doesn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Login_WrongLogin, errorMsg, "Wrong text for error message if credentials are wrong"));
        }
    }
}
