﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    class PrepaidMobileChangeTariff : TestSet
    {
        LoginPage loginPage;
        PrepaidMobileChangeTariffPage page;
        private string testUser;
        private string testPassword;
        private string testServiceId;

        public PrepaidMobileChangeTariff()
        {
            testUser = SpecificData.PrepaidMobileChangeTariffUser[0, 0];
            testPassword = SpecificData.PrepaidMobileChangeTariffUser[0, 1];
            testServiceId = SpecificData.PrepaidMobileChangeTariffUser[0, 2];
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test Is interrupted", testUser));
            string pageUrl = string.Format(PageUrls.PrepaidMobileChangeTariffUrl, testServiceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Change Tariff page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PrepaidMobileChangeTariffPage(driver);
            page.isPageLoaded();

            _assertsAccumulator = new AssertsHelper();
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobile_CheckTariffElements()
        {
            page.isPageLoaded();

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            Assert.IsTrue(page.GetTariffsCount() > 0, "There is no one available tariff. Test is interrupted");

            page.CheckElements(_assertsAccumulator);
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobile_CheckPopupElements()
        {
            page.isPageLoaded();

            Assert.IsTrue(page.GetTariffsCount() > 0, "Failed: there is no one available tariff");

            page.ClickTariffButton(0);
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupDisplayed(), "Failed: conformation popup is not opened. Test is interrupted");


        }

        [Category("TestFlows")]
        [Test]
        public void PrepaidMobile_ShouldChangeTariff()
        {
            page.isPageLoaded();

            Assert.IsTrue(page.GetTariffsCount() > 0, "There is no one available tariff. Test is interrupted");

            int tariffsCount = page.GetTariffsCount();
            for (int i = 0; i < tariffsCount; i++)
            {
                var oldTariff = page.GetActiveTariffName();
                var newTariff = page.GetTariffName(i);
                page.ClickTariffButton(i);
                page.isPageLoaded();

                Assert.IsTrue(page.isPopupDisplayed(), "Failed: conformation popup is not displayed. Test is interrupted");
                page.ClickPopupButton();
                page.isPageLoaded();

                _assertsAccumulator.Accumulate(() => Assert.AreEqual(page.GetActiveTariffName(), newTariff, 
                    string.Format("Failed for {0}: tariff is not changed in navigation sidebar", i)));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(tariffsCount, page.GetTariffsCount(), string.Format("Count of available tariffs failed for {0}", i)));

                for (int j = 0; j < tariffsCount; j++)
                {
                    _assertsAccumulator.Accumulate(() => Assert.IsFalse(page.GetTariffName(j).Equals(newTariff), 
                        string.Format("Failed: tariff {0} just selected for activation, is in the list of available tariffs", newTariff)));
                }

                int index = -1;
                for (int j = 0; j < tariffsCount; j++)
                {
                    if (page.GetTariffName(j).Equals(oldTariff))
                    {
                        index = j;
                        break;
                    }
                }
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(index >= 0,
                    string.Format("Failed: previously activated tariff {0} is not in the list of available tariffs", newTariff)));
            }
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
