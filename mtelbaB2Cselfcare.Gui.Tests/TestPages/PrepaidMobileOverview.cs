﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    class PrepaidMobileOverview : TestSet
    {
        LoginPage loginPage;
        PrepaidMobileOverviewPage page;
        private string testUser;
        private string testPassword;
        private string testServiceId;
        private string testServiceState;

        public PrepaidMobileOverview()
        {
            testUser = SpecificData.PrepaidMobileResourcesUser[0, 0];
            testPassword = SpecificData.PrepaidMobileResourcesUser[0, 1];
            testServiceId = SpecificData.PrepaidMobileResourcesUser[0, 2];
        }

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));

            string pageUrl = string.Format(PageUrls.PrepaidMobileOverviewUrl, testServiceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Overview page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PrepaidMobileOverviewPage(driver);
            page.isPageLoaded();
            testServiceState = page.GetServiceStatus();

            _assertsAccumulator = new AssertsHelper();
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobileResources_CheckResoucesAndBannerElements()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateResources);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckResources(_assertsAccumulator, testServiceId);

            var isBannerShown = page.isBannerShown();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isBannerShown, "Banner failed: is not shown"));
            if (isBannerShown)
            {
                page.CheckBannerResources(_assertsAccumulator);
            }

            _assertsAccumulator.Accumulate(() => Assert.That(page.isDeactivationSwitcherAvailable(), "Switcher to deactivate the tariff plan isn't shown"));
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobileResources_CheckPopupElements()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateResources);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            page.ClickDeactivationSwitcher();
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupDisplayed(), "Failed: confirmation popup is not opened. Test is interrupted");
            page.CheckLabelsPopupDeactivate(_assertsAccumulator, page.GetTariffPlanName());
            page.ClickPopupButtonCancel();
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobileAvailableTariffPlans_CheckPlanCardsAndBannerElements()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateTariffPlanToActivate);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));

            int availableTariffPlans = page.GetAvailableTariffPlansCnt();
            _assertsAccumulator.Accumulate(() => Assert.That(availableTariffPlans > 0, "There is no available tariff plans"));
            if (availableTariffPlans > 0)
            {
                page.CheckTariffPlanCard(_assertsAccumulator);
            }

            var isBannerShown = page.isBannerShown();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isBannerShown, "Banner failed: is not shown"));
            if (isBannerShown)
            {
                page.CheckBannerAvailableTariffPlans(_assertsAccumulator);
            }
        }

        [Category("TestPages")]
        [Test]
        public void PrepaidMobileAvailableTariffPlans_CheckPopupElements()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateTariffPlanToActivate);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            Assert.That(page.GetAvailableTariffPlansCnt() > 0, "There is no available tariff plans. Test is interrupted");
            page.ClickTariffPlanCardButton(0);
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupDisplayed(), "Failed: confirmation popup is not opened. Test is interrupted");
            page.CheckLabelsPopupActivate(_assertsAccumulator, page.GetAvailableTariffPlanName(0), page.GetAvailableTariffPlanPrice(0));
            page.ClickPopupButtonCancel();
        }

        [Category("TestFlows")]
        [Test]
        public void PrepaidMobileResources_ShouldBeDeactivated()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateResources);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            var activeTariffPlanName = page.GetTariffPlanName();
            Assert.IsTrue(page.DeactivateTariffPlan(), "Deactivation failed");

            var expectedMsg = string.Format(page.GetNotificationMsg("PlanDeactivated"), activeTariffPlanName);
            var waitNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Message about tariff plan deactivation failed: notification is not shown"));
            if (waitNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Message about tariff plan deactivation failed: is not correct"));
            }
            page.isPageLoaded();

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(page.ServiceStateTariffPlanToActivate, page.GetServiceStatus(),
                    string.Format("Failed: service state is not {0}", page.ServiceStateTariffPlanToActivate)));
        }

        [Category("TestFlows")]
        [Test]
        public void PrepaidMobileAvailableTariffPlans_ShouldBeActivated()
        {
            var actionSuccessfull = page.ChangeServiceStatusTo(page.ServiceStateTariffPlanToActivate);
            Assert.IsTrue(actionSuccessfull, "Test page has inappropriate state. Test is interrupted");

            Assert.That(page.GetAvailableTariffPlansCnt() > 0, "There is no available tariff plans. Test is interrupted");

            int index = Helper.GetRandomIndex(0, page.GetAvailableTariffPlansCnt());
            var selectedTariffPlanName = page.GetAvailableTariffPlanName(index);
            Assert.IsTrue(page.ActivateTariffPlan(index), "Activation failed");

            var expectedMsg = string.Format(page.GetNotificationMsg("PlanActivated"), selectedTariffPlanName);
            var waitNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Message about tariff plan activation failed: notification is not shown"));
            if (waitNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Message about tariff plan activation failed: is not correct"));
            }
            page.isPageLoaded();

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(page.ServiceStateResources, page.GetServiceStatus(),
                string.Format("Failed: service state is not {0}", page.ServiceStateResources)));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(selectedTariffPlanName, page.GetTariffPlanName(),
                "Failed: the selected for activation tariff plan is not activated"));
        }

        [Category("TestPages")]
        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckPrepaidMobileGraceActive3DaysUser))]
        public void PrepaidMobileGraceActive3Days_CheckTextsAndBannerElementsOnPage(string user, string password, string serviceId)
        {
            string pageUrl = string.Format(PageUrls.PrepaidMobileOverviewUrl, serviceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Overview page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PrepaidMobileOverviewPage(driver);
            page.isPageLoaded();
            Assert.AreEqual(page.ServiceStateTariffPlanToActivate, page.GetServiceStatus(), "Test page has inappropriate state. Test is interrupted");

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabelsGraceActive3Days(_assertsAccumulator);

            var isBannerShown = page.isBannerShown();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isBannerShown, "Banner failed: is not shown"));
            if (isBannerShown)
            {
                page.CheckBannerGraceActive3Days(_assertsAccumulator);
            }
        }

        [Category("TestPages")]
        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckPrepaidMobileGraceUser))]
        public void PrepaidMobileGrace_CheckTextsAndPopupElementsOnPage(string user, string password, string serviceId)
        {
            string pageUrl = string.Format(PageUrls.PrepaidMobileOverviewUrl, serviceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Overview page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PrepaidMobileOverviewPage(driver);
            page.isPageLoaded();
            Assert.AreEqual(page.ServiceStateGrace, page.GetServiceStatus(), "Test page has inappropriate state. Test is interrupted");

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabelsGrace(_assertsAccumulator);

            page.ClickGraceActivateButton();
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupDisplayed(), "Failed: confirmation popup is not opened. Test is interrupted");
            page.CheckLabelsPopupGrace(_assertsAccumulator);
            page.ClickPopupButtonCancel();
        }

        [Category("TestPages")]
        [Category("TestFlows")]
        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckPrepaidMobileGraceUser))]
        public void PrepaidMobileGrace_ShouldNotifyNotEnoughCreditForActivation(string user, string password, string serviceId)
        {
            string pageUrl = string.Format(PageUrls.PrepaidMobileOverviewUrl, serviceId);
            Assert.IsTrue(TestsHelper.OpenPage(driver, pageUrl), string.Format("Overview page is not opened, expected {0}, but is {1}", pageUrl, driver.Url));

            page = new PrepaidMobileOverviewPage(driver);
            page.isPageLoaded();
            Assert.AreEqual(page.ServiceStateGrace, page.GetServiceStatus(), "Test page has inappropriate state. Test is interrupted");

            page.ClickGraceActivateButton();
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupGraceActivateDisplayed(), "Failed: confirmation popup is not opened. Test is interrupted");
            page.ClickPopupButton();

            page.isPageLoaded();
            Assert.IsTrue(page.isPopupGraceWarningDisplayed(), "Failed: warning popup is not opened. Test is interrupted");

            page.CheckLabelsPopupGraceWarning(_assertsAccumulator);
            page.ClickGraceWarningPopupClose();
        }

        [TearDown]
        public void logout()
        {
            page.ChangeServiceStatusTo(testServiceState);
            page.LogOut();
        }
    }
}
