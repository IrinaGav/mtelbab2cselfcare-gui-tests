﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class ForgotPasswordLinkExpired : TestSet
    {
        ForgotPasswordLinkExpiredPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ForgottenPasswordLinkExpiredUrl),
                string.Format("Forgotten Password page is not opened, expected {0}, but is {1}", PageUrls.ForgottenPasswordLinkExpiredUrl, driver.Url));

            page = new ForgotPasswordLinkExpiredPage(driver);

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Test]
        public void ForgotPasswordLinkExpired_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo doesn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLinkExpiredIconDisplayed(), "Page icon doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void ForgotPasswordLinkExpired_CheckButtonRedirection()
        {
            page.ClickButton();
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isPageOpened(driver, PageUrls.ForgottenPasswordUrl), 
                "Button click failed: Forgotten Password page doesn't open"));
        }
    }
}
