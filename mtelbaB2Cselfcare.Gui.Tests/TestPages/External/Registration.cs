﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class Registration : TestSet
    {
        RegistrationPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}. Test is interrupted", PageUrls.RegistrationUrl, driver.Url));

            page = new RegistrationPage(driver);
            Assert.IsTrue(page.isPageLoaded(), "Registration page is not loaded. Test is interrupted");

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Test]
        public void Registration_CheckTextsAndElementsOnPage()
        {
            driver.Navigate().Refresh();
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
            page.CheckTooltips(driver, _assertsAccumulator);
        }

        [Test]
        public void Registration_CheckMandatoryFields()
        {
            driver.Navigate().Refresh();
            page.CheckMandatoryInputs(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidInputPhone))]
        public void Registration_CheckInvalidInputPhone(string inputValue)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(inputValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsDisabled(), "Phone input failed: the link SMS send isn't disabled"));

            var errorMsg = page.GetMsgInputPhone();

            _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Input Phone failed: the error message isn't shown"));
            if (errorMsg.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_InvalidPhone, errorMsg, "Wrong text for error message if phone is incorrect"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckValidInputPhone))]
        public void Registration_CheckValidInputPhone(string inputValue)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(inputValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckValidInputPhone))]
        public void Registration_CheckSendingSms(string inputValue)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(inputValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));
            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("SmsSent");
            var notification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(notification.Length > 0, "Mobile service SMS send failed: notification is not shown"));
            if (notification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, notification, "Mobile service SMS send failed: notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidOtp))]
        public void Registration_CheckInvalidOtp(string phoneValue, string otpCode)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var waitNotification = page.WaitNotification(page.GetNotificationMsg("SmsSent"));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Failed: notification SmS sent is not shown"));
            if (waitNotification.Length > 0)
            {
                page.CloseNotification();
            }

            page.EnterOtp(otpCode);
            page.ClickMobileServiceButton();

            var expectedMsg = page.GetNotificationMsg("OtpWrongFormat");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service otp entering failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Mobile service otp entering failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckLimitOtpExceeded))]
        public void Registration_CheckLimitOtpExceeded(string phoneValue, int limitOtpSending)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            for (int i = 0; i < limitOtpSending; i++)
            {
                page.ClickLinkSmsSend();
                page.isPageLoaded();
            }

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("LimitOtpCode");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service sms sending failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                       "Mobile service sms sending failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidMobile))]
        public void Registration_CheckInvalidMobileService(string phoneValue, string otpCode, string msg)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            page.EnterOtp(otpCode);
            var buttonEnabled = page.isMobileServiceButtonEnabled();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled"));

            if (buttonEnabled)
            {
                page.ClickMobileServiceButton();
                page.isPageLoaded();

                var expectedMsg = page.GetNotificationMsg(msg);
                var waitErrorNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service failed: error notification is not shown"));
                if (waitErrorNotification.Length > 0)
                {
                    _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                        "Mobile service failed: error notification is not correct"));
                }
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidFixed))]
        public void Registration_CheckInvalidFixedService(string subscriberCode, string referenceNumber, string msg)
        {
            driver.Navigate().Refresh();

            page.EnterSubscription(subscriberCode);
            page.EnterNumber(referenceNumber);
            var buttonEnabled = page.isFixedServiceButtonEnabled();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(buttonEnabled, "Fixed service inputs failed: button is not enabled"));

            if (buttonEnabled)
            {
                page.ClickFixedServiceButton();
                page.isPageLoaded();

                var expectedMsg = page.GetNotificationMsg(msg);
                var waitErrorNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Fixed service inputs failed: error notification is not shown"));
                if (waitErrorNotification.Length > 0)
                {
                    _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Fixed service inputs failed: error notification is not correct"));
                }
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckMobileAccountExistMain))]
        public void Registration_ShouldReturnExistAccountMain(string phoneValue, string otpCode, string user, string msg)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(phoneValue);

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            page.EnterOtp(otpCode);
            Assert.IsTrue(page.isMobileServiceButtonEnabled(), "Mobile service inputs failed: button is not enabled. Test is interrupted");

            page.ClickMobileServiceButton();
            page.isPageLoaded();

            RegistrationExistAccountPage accountExistPage = new RegistrationExistAccountPage(driver);

            Assert.IsTrue(accountExistPage.isFormPageLoaded(), "Continue Registration failed: ExistAccountForm is not loaded. Test is interrupted");

            accountExistPage.CheckMandatoryInputs(_assertsAccumulator);
            accountExistPage.CheckLabelsExistMain(_assertsAccumulator, user);

            accountExistPage.EnterPassword("123456789");
            accountExistPage.ClickButtonLogin();
            accountExistPage.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("InvalidPassword");
            var waitErrorNotification = accountExistPage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Registration Exist account. Entering wrong password failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                    "Registration Exist account. Entering wrong password failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckMobileAccountExistNotMain))]
        public void Registration_ShouldReturnExistAccountNotMain(string phoneValue, string otpCode, string user, string msg)
        {
            driver.Navigate().Refresh();
            page.EnterPhone(phoneValue);

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            page.EnterOtp(otpCode);
            Assert.IsTrue(page.isMobileServiceButtonEnabled(), "Mobile service inputs failed: button is not enabled. Test is interrupted");

            page.ClickMobileServiceButton();
            page.isPageLoaded();

            RegistrationExistAccountPage accountExistPage = new RegistrationExistAccountPage(driver);

            Assert.IsTrue(accountExistPage.isFormPageLoaded(), "Continue Registration failed: ExistAccountForm is not loaded. Test is interrupted");

            accountExistPage.CheckMandatoryInputs(_assertsAccumulator);
            accountExistPage.CheckLabelsExistNotMain(_assertsAccumulator, user);

            accountExistPage.EnterPassword("123456789");
            accountExistPage.ClickButtonLogin();
            accountExistPage.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("InvalidPassword");
            var waitErrorNotification = accountExistPage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Registration Exist account. Entering wrong password failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                "Registration Exist account. Entering wrong password failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckFixedNewAccount))]
        public void Registration_CheckNewAccountFormLoaded(string subscriberCode, string referenceNumber)
        {
            driver.Navigate().Refresh();

            page.EnterSubscription(subscriberCode);
            page.EnterNumber(referenceNumber);
            var buttonEnabled = page.isFixedServiceButtonEnabled();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(buttonEnabled, "Fixed service inputs failed: button is not enabled"));

            RegistrationNewAccountPage newAccountPage = new RegistrationNewAccountPage(driver);
            bool newAccountFormLoaded = false;
            if (buttonEnabled)
            {
                page.ClickFixedServiceButton();
                page.isPageLoaded();

                newAccountFormLoaded = newAccountPage.isFormPageLoaded();
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(newAccountFormLoaded, "Continue Registration failed: NewAccountForm is not loaded"));
            }

            if (newAccountFormLoaded)
            {
                newAccountPage.CheckMandatoryInputs(_assertsAccumulator);
                newAccountPage.CheckLabels(_assertsAccumulator);

                foreach (var item in CommonData.InvalidEmail)
                {
                    newAccountPage.EnterEmail("");
                    newAccountPage.EnterEmail(item);

                    var errorMsg = newAccountPage.GetMsgInputEmail();

                    _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Input Email failed: the error message isn't shown, if email is incorrect"));
                    if (errorMsg.Length > 0)
                    {
                        _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_InvalidEmail, errorMsg, "Wrong text for error message, if email is incorrect"));
                    }
                }
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(newAccountPage.isButtonDisabled(), "New Account entering data failed: button is enabled"));
            }
        }

    }
}
