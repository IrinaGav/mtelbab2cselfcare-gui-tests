﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class ForgottenPassword : TestSet
    {
        ForgottenPasswordPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ForgottenPasswordUrl),
                string.Format("Forgotten Password page is not opened, expected {0}, but is {1}", PageUrls.ForgottenPasswordUrl, driver.Url));

            page = new ForgottenPasswordPage(driver);
            Assert.IsTrue(page.isPageLoaded(), "Forgotten Password page is not loaded");

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Test]
        public void ForgottenPassword_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void ForgottenPassword_CheckMandatoryFields()
        {
            page.CheckMandatoryInputs(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(LoginData), nameof(LoginData.CheckInvalidInputEmail))]
        public void ForgottenPassword_CheckInvalidInputEmail(string inputValue)
        {
            page.EnterEmail(inputValue);
            var errorMsg = page.GetMsgInputEmail();

            _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Field Email failed: the error message isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_InvalidEmail, errorMsg, "Wrong text for error message if email is incorrect"));
        }

        [Test, TestCaseSource(typeof(LoginData), nameof(LoginData.CheckNotExistentEmail))]
        public void ForgottenPassword_CheckNotExistentEmail(string inputValue)
        {
            page.EnterEmail(inputValue);
            page.ClickButton();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("NotExist");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Failed: error notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Failed: error notification is not correct"));
        }
    }
}
