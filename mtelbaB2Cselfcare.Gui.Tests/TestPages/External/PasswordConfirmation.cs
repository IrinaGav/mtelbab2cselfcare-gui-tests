﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class PasswordConfirmation : TestSet
    {
        PasswordConfirmationPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.PasswordConfirmationUrl),
                string.Format("Password Confirmation page is not opened, expected {0}, but is {1}", PageUrls.PasswordConfirmationUrl, driver.Url));

            page = new PasswordConfirmationPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void PasswordConfirmation_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo doesn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isEmailIconDisplayed(), "Page icon doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }
    }
}
