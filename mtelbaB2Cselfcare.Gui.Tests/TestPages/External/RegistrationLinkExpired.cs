﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class RegistrationLinkExpired : TestSet
    {
        RegistrationLinkExpiredPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.RegistrationLinkExpiredUrl), 
                string.Format("Registration Link Expired page is not opened, expected {0}, but is {1}", PageUrls.RegistrationLinkExpiredUrl, driver.Url));
            page = new RegistrationLinkExpiredPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void RegistrationLinkExpired_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo doesn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLinkExpiredIconDisplayed(), "Page icon doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void RegistrationLinkExpired_CheckButtonRedirection()
        {
            var registrationPage = new RegistrationPage(driver);

            page.ClickButton();
            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.RegistrationUrl),
                string.Format("Registration page is not opened, expected {0}, but is {1}", PageUrls.RegistrationUrl, driver.Url));
        }
    }
}
