﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class NewPassword : TestSet
    {
        NewPasswordPage page;

        [SetUp]
        public void SetUp()
        {
            page = new NewPasswordPage(driver);

            _assertsAccumulator = new Helpers.AssertsHelper();
        }

        [Test]
        public void NewPassword_ShouldRedirectToLinkExpiredIfNoToken()
        {
            TestsHelper.OpenPage(driver, PageUrls.NewPasswordUrl);

            Assert.IsTrue(TestsHelper.isPageOpened(driver, PageUrls.ForgottenPasswordLinkExpiredUrl), 
                string.Format("New Password page is not redirected to Link Expired page, expected {0}, but is {1}", PageUrls.ForgottenPasswordLinkExpiredUrl, driver.Url));
        }
    }
}
