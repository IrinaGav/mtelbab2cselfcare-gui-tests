﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class RegistrationConfirmation : TestSet
    {
        RegistrationConfirmationPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.RegistrationConfirmationUrl),
                string.Format("Registration Confirmation page is not opened, expected {0}, but is {1}", PageUrls.RegistrationConfirmationUrl, driver.Url));
            page = new RegistrationConfirmationPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void RegistrationConfirmation_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo doesn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isEmailIconDisplayed(), "Page icon doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void RegistrationConfirmation_CheckButtonRedirectionWithoutTokenNotAvailable()
        {
            page.ClickButton();
            var notificationText = page.GetNotificationText();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(notificationText.Length > 0, "Fixed service inputs failed: error notification is not shown"));
            if (notificationText.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(page.GetNotificationMsg("NoToken", ""), notificationText,
                       "Fixed service inputs failed: error notification is not correct"));
            }
        }
    }
}
