﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class PasswordChanged : TestSet
    {
        PasswordChangedPage page;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.PasswordChangedUrl),
                string.Format("Password Changed page is not opened, expected {0}, but is {1}", PageUrls.PasswordChangedUrl, driver.Url));

            page = new PasswordChangedPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void PasswordChanged_CheckTextsAndElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isSuccessIconDisplayed(), "Page icon doesn't shown"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void PasswordChanged_CheckButtonRedirection()
        {
            var loginPage = new LoginPage(driver);

            page.ClickButton();

            Assert.That(TestsHelper.isPageOpened(driver, PageUrls.LoginUrl), "Button click failed: Login page doesn't open");
        }
    }
}

