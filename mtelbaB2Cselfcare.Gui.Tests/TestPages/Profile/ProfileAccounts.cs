﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    [Category("TestProfile")]
    class ProfileAccounts : TestSet
    {
        LoginPage loginPage;
        ProfileAccountsPage page;
        private string testUser;
        private string testPassword;

        public ProfileAccounts()
        {
            testUser = SpecificData.GeneralTestUser[0, 0];
            testPassword = SpecificData.GeneralTestUser[0, 1];
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.AccountsUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.AccountsUrl, driver.Url));

            page = new ProfileAccountsPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void Accounts_CheckTextsAndElementsOnPage()
        {
            driver.Navigate().Refresh();

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabels(_assertsAccumulator);
            page.CheckLinksCount(_assertsAccumulator);

            page.ClickUnlink(0);
            page.isPageLoaded();

            var popupDisplayed = page.isPopupDisplayed();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(popupDisplayed, "Failed: confirmation popup is not opened"));

            if (popupDisplayed)
            {
                page.CheckLabelsPopup(_assertsAccumulator);
                page.ClickPopupButtonCancel();
            }
        }

        [Test]
        public void AddService_CheckTextsAndElementsOnForm()
        {
            driver.Navigate().Refresh();

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.CheckLabelsAddService(_assertsAccumulator);
            addServicePage.CheckMandatoryInputsAddService(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(ProfileAccountsData), nameof(ProfileAccountsData.CheckMobileAccountIsConnected))]
        public void AddMobileService_ShouldReturnIsAlreadyConnected(string mobile, string otpCode)
        {
            driver.Navigate().Refresh();

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.activateAddMobileServiceTab();
            addServicePage.EnterPhone(mobile);
            addServicePage.ClickLinkSmsSend();
            addServicePage.isPageLoaded();

            addServicePage.EnterOtp(otpCode);

            var buttonEnabled = addServicePage.isMobileServiceButtonEnabled();
            Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled. Test is interrupted");

            addServicePage.ClickMobileServiceButton();
            addServicePage.isPageLoaded();

            var expectedMsg = addServicePage.GetNotificationMsg("ServiceIsConnected");
            var waitErrorNotification = addServicePage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Adding already connected service failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                       "Adding already connected service failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileAccountsData), nameof(ProfileAccountsData.CheckMobileAccountExistMain))]
        public void AddMobileService_ShouldReturnExistMain(string mobile, string otpCode, string user, string password)
        {
            driver.Navigate().Refresh();

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.activateAddMobileServiceTab();
            addServicePage.EnterPhone(mobile);
            addServicePage.ClickLinkSmsSend();
            addServicePage.isPageLoaded();

            addServicePage.EnterOtp(otpCode);

            var buttonEnabled = addServicePage.isMobileServiceButtonEnabled();
            Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled. Test is interrupted");

            addServicePage.ClickMobileServiceButton();
            addServicePage.isPageLoaded();

            var expectedMsg = addServicePage.GetNotificationMsg("MainService");
            var waitErrorNotification = addServicePage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Adding main connected service failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                       "Adding main connected service failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileAccountsData), nameof(ProfileAccountsData.CheckMobileExistNotMainData))]
        public void AddMobileService_CheckTextsOnServiceExistNotMain(string mobile, string otpCode, string user, string password)
        {
            driver.Navigate().Refresh();

            page.OpenAddService();

            var addServicePage = new ProfileAddServicePage(driver);
            Assert.IsTrue(addServicePage.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            addServicePage.activateAddMobileServiceTab();
            addServicePage.EnterPhone(mobile);
            addServicePage.ClickLinkSmsSend();
            addServicePage.isPageLoaded();

            addServicePage.EnterOtp(otpCode);

            var buttonEnabled = addServicePage.isMobileServiceButtonEnabled();
            Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled. Test is interrupted");

            addServicePage.ClickMobileServiceButton();
            addServicePage.isPageLoaded();

            addServicePage.CheckLabelsAddServiceExist(_assertsAccumulator, user);

            addServicePage.EnterPassword("12345678");
            addServicePage.ClickAddServiceExistButton();

            var expectedMsg = addServicePage.GetNotificationMsg("InvalidPassword");
            var waitErrorNotification = addServicePage.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Adding existent service with invalid password failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                       "Adding existent service with invalid password failed: error notification is not correct"));
            }
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
