﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    [Category("TestProfile")]
    class ProfileDeleteAccount : TestSet
    {
        ProfileDeleteAccountPage page;
        LoginPage loginPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckProfileUser))]
        public void ProfileDeleteAccount_CheckTexts(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileDeleteAccountUrl),
                string.Format("ProfileDeleteAccount page is not opened, expected {0}, but is {1}", PageUrls.ProfileDeleteAccountUrl, driver.Url));

            page = new ProfileDeleteAccountPage(driver);

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabels(_assertsAccumulator);

            page.ClickPageButton();
            page.isPageLoaded();

            Assert.IsTrue(page.isPopupDisplayed(), "Failed: confirmation popup is not opened. Test is interrupted");

            page.CheckLabelsPopup(_assertsAccumulator);
            page.ClickPopupButtonCancel();
        }

        [TearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
