﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    [Category("TestProfile")]
    class ProfileChangePassword : TestSet
    {
        ProfileChangePasswordPage page;
        LoginPage loginPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileUserInputs))]
        public void ProfileChangePassword_CheckTextsElementsAndMandatoryOnPage(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password),"User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabels(_assertsAccumulator);
            page.CheckMandatoryInputs(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckInvalidNewPassword))]
        public void ProfileChangePassword_CheckInvalidInputNewPassword(string user, string password, string[,] newPassword)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            for (int i = 0; i < newPassword.GetLength(0); i++)
            {
                driver.Navigate().Refresh();
                page.isPageLoaded();

                page.EnterOldPassword(password);
                page.EnterNewPassword(newPassword[i, 0]);
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(newPassword[i, 1], page.GetMsgInputNewPassword(), "Failed: error message is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckValidNewPassword))]
        public void ProfileChangePassword_CheckValidInputNewPassword(string user, string password, string[] newPassword)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            foreach (var item in newPassword)
            {
                driver.Navigate().Refresh();
                page.isPageLoaded();

                page.EnterOldPassword(password);
                page.EnterNewPassword(item);
                var errorMsg = page.GetMsgInputNewPassword();
                _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length == 0, "Failed: error message is shown - " + errorMsg));
            }
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileUserInputs))]
        public void ProfileChangePassword_CheckInvalidInputConfirmPassword(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            page.EnterOldPassword(password);
            page.EnterNewPassword(password + password);
            page.EnterConfirmPassword(password + "?");
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.PasswordsNotMatch, page.GetMsgInputConfirmPassword(), "Failed: error message is not correct"));
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileUserInputs))]
        public void ProfileChangePassword_CheckInvalidInputOldPassword(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            page.EnterOldPassword(password + "1");
            page.EnterNewPassword(password + "1");
            page.EnterConfirmPassword(password + "1");

            page.ClickButtonChangePassword();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("InvalidOldPassword");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileUserInputs))]
        public void ProfileChangePassword_CheckNewPasswordContainsUserName(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            page.EnterOldPassword(password);
            var newPassword = (user.Substring(0, user.IndexOf("@")));
            page.EnterNewPassword(newPassword);
            page.EnterConfirmPassword(newPassword);

            page.ClickButtonChangePassword();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("UserNameInPassword");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Failed: error notification is not shown"));
            if (waitErrorNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileChangePasswordUser))]
        public void ProfileChangePassword_CheckNewPasswordDiffersOldPassword(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            var expectedMsgOldPassword = page.GetNotificationMsg("PasswordIsOld");
            var expectedMsg = page.GetNotificationMsg("PasswordIsChanged");

            string newPassword = password;

            for (int i = 0; i < CommonData.OldPasswordInMemory; i++)
            {
                var oldPassword = newPassword;
                page.EnterOldPassword(oldPassword);
                page.EnterNewPassword(password);
                page.EnterConfirmPassword(password);

                page.ClickButtonChangePassword();
                page.isPageLoaded();

                var waitErrorNotification = page.WaitNotification(expectedMsgOldPassword);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, (i + 1) + "changing failed: error notification is not shown"));
                if (waitErrorNotification.Length > 0)
                {
                    _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsgOldPassword, waitErrorNotification, (i + 1) + "changing failed: error notification is not correct"));
                }

                newPassword = password + i;

                page.EnterNewPassword("");
                page.EnterNewPassword(newPassword);
                page.EnterConfirmPassword("");
                page.EnterConfirmPassword(newPassword);

                page.ClickButtonChangePassword();
                page.isPageLoaded();

                var waitNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, (i + 1) + "changing failed: success notification is not shown"));
                if (waitErrorNotification.Length > 0)
                {
                    _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, (i + 1) + "changing failed: success notification is not correct"));
                }
            }
            page.EnterOldPassword(newPassword);
            page.EnterNewPassword(password);
            page.EnterConfirmPassword(password);

            page.ClickButtonChangePassword();
            page.isPageLoaded();

            var expectedSuccessMsg = page.GetNotificationMsg("PasswordIsChanged");
            var waitSuccessNotification = page.WaitNotification(expectedSuccessMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitSuccessNotification.Length > 0, "Failed: success notification is not shown"));
            if (waitSuccessNotification.Length > 0)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedSuccessMsg, waitSuccessNotification, "Failed: success notification is not correct"));
            }
        }

        [Category("TestFlows")]
        [Test, TestCaseSource(typeof(ProfileChangePasswordData), nameof(ProfileChangePasswordData.CheckProfileChangePasswordFlowUser))]
        public void ProfileChangePassword_ShouldChangePasswordLoginThenReturnPassword(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            page = new ProfileChangePasswordPage(driver);

            string newPassword = password + password;
            page.EnterOldPassword(password);
            page.EnterNewPassword(newPassword);
            page.EnterConfirmPassword(newPassword);

            page.ClickButtonChangePassword();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("PasswordIsChanged");
            var waitNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Message about changing failed: success notification is not shown"));

            page.LogOut();

            loginPage = new LoginPage(driver);
            Assert.IsTrue(loginPage.loginByUser(user, newPassword), "User cannot log in after password changing. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangePasswordUrl),
                string.Format("ProfileChangePassword page is not opened after password changing, expected {0}, but is {1}", PageUrls.ProfileChangePasswordUrl, driver.Url));

            for (int i = 0; i < CommonData.OldPasswordInMemory; i++)
            {
                var oldPassword = newPassword;
                newPassword = password + i;
                page.EnterOldPassword(oldPassword);
                page.EnterNewPassword(newPassword);
                page.EnterConfirmPassword(newPassword);

                page.ClickButtonChangePassword();
                page.isPageLoaded();

                waitNotification = page.WaitNotification(expectedMsg);
            }
            page.EnterOldPassword(newPassword);
            page.EnterNewPassword(password);
            page.EnterConfirmPassword(password);

            page.ClickButtonChangePassword();
            page.isPageLoaded();

            var expectedSuccessMsg = page.GetNotificationMsg("PasswordIsChanged");
            var waitSuccessNotification = page.WaitNotification(expectedSuccessMsg);
        }

        [TearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
