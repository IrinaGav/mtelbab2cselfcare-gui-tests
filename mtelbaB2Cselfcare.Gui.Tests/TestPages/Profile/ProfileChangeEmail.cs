﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    [Category("TestProfile")]
    class ProfileChangeEmail : TestSet
    {
        ProfileChangeEmailPage page;
        LoginPage loginPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckProfileUser))]
        public void ProfileChangeEmail_CheckTextsElementsAndMandatoryOnPage(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangeEmailUrl), 
                string.Format("ProfileChangeEmail page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangeEmailUrl, driver.Url));

            page = new ProfileChangeEmailPage(driver);

            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabels(_assertsAccumulator);
            page.CheckMandatoryInputs(_assertsAccumulator);
        }

        [Test, TestCaseSource(typeof(ProfileChangeEmailData), nameof(ProfileChangeEmailData.CheckInvalidEmail))]
        public void ProfileChangeEmail_CheckInvalidInputEmail(string user, string password, string[] inputValue)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangeEmailUrl),
                string.Format("ProfileChangeEmail page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangeEmailUrl, driver.Url));

            page = new ProfileChangeEmailPage(driver);

            foreach (var item in inputValue)
            {
                page.EnterEmail("");
                page.EnterEmail(item);

                var errorMsg = page.GetMsgInputEmail();

                _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Input Email failed: the error message isn't shown"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_InvalidEmail, errorMsg, "Wrong text for error message if email is incorrect"));
            }
        }

        [Test, TestCaseSource(typeof(ProfileChangeEmailData), nameof(ProfileChangeEmailData.CheckEmailExists))]
        public void ProfileChangeEmail_CheckEmailExists(string user, string password, string[] inputValue)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangeEmailUrl),
                string.Format("ProfileChangeEmail page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangeEmailUrl, driver.Url));

            page = new ProfileChangeEmailPage(driver);

            foreach (var item in inputValue)
            {
                page.EnterEmail("");
                page.EnterPassword("");

                page.EnterEmail(item);
                page.EnterPassword(password);

                page.ClickButtonChangeEmail();
                page.isPageLoaded();

                var errorMsg = page.GetMsgInputEmail();

                _assertsAccumulator.Accumulate(() => Assert.That(errorMsg.Length > 0, "Failed: the error message isn't shown"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(PageMessages.Msg_EmailExists, errorMsg, "Wrong text for error message if email exists in the system"));
            }
        }

        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckProfileUser))]
        public void ProfileChangeEmail_CheckInvalidInputPassword(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangeEmailUrl),
                string.Format("ProfileChangeEmail page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangeEmailUrl, driver.Url));

            page = new ProfileChangeEmailPage(driver);

            page.EnterEmail("autotest.new.email" + user);
            page.EnterPassword(password + "1");

            page.ClickButtonChangeEmail();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("InvalidPassword");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Failed: error notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Failed: error notification is not correct"));
        }

        [Category("TestFlows")]
        [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.CheckProfileUser))]
        public void ProfileChangeEmail_ShouldChangeEmailSeeConfirmationAndSendEmailAgain(string user, string password)
        {
            Assert.IsTrue(loginPage.loginByUser(user, password), "User is not logged in. Test Is interrupted");
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.ProfileChangeEmailUrl),
                string.Format("ProfileChangeEmail page is not opened, expected {0}, but is {1}", PageUrls.ProfileChangeEmailUrl, driver.Url));

            page = new ProfileChangeEmailPage(driver);

            var newEmail = "autotest.new.email" + user;
            page.EnterEmail(newEmail);
            page.EnterPassword(password);

            page.ClickButtonChangeEmail();
            page.isPageLoaded();

            var popupDisplayed = page.isPopupDisplayed();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(popupDisplayed, "Failed: confirmation popup is not opened"));

            if (popupDisplayed)
            {
                page.CheckLabelsPopup(_assertsAccumulator, newEmail);

                page.ClickPopupButton();

                var expectedMsg = string.Format(page.GetNotificationMsg("EmailSent"), newEmail);
                var waitNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Failed: email sent notification is not shown"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitNotification, "Failed: email sent notification is not correct"));

                page.ClickCloseButtonPopup();
            }
        }

        [TearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
