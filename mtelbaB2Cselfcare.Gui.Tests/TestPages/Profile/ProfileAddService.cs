﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    [Category("TestProfile")]
    class ProfileAddService : TestSet
    {
        LoginPage loginPage;
        ProfileAccountsPage profileAccountsPage;
        ProfileAddServicePage page;
        Registration registrationTests = new Registration();
        private string testUser;
        private string testPassword;

        public ProfileAddService()
        {
            testUser = SpecificData.GeneralTestUser[0, 0];
            testPassword = SpecificData.GeneralTestUser[0, 1];
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.AccountsUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.AccountsUrl, driver.Url));

            profileAccountsPage = new ProfileAccountsPage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [SetUp]
        public void SetUp()
        {
            driver.Navigate().Refresh();
            profileAccountsPage.OpenAddService();

            page = new ProfileAddServicePage(driver);
            Assert.IsTrue(page.isAddServiceOpened(), "Failed: Add service form is not opened. Test is interrupted");

            page.isPageLoaded();
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckValidInputPhone))]
        public void AddMobileService_CheckValidInputPhone(string inputValue)
        {
            page.activateAddMobileServiceTab();
            page.EnterPhone(inputValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckValidInputPhone))]
        public void AddMobileService_CheckSendingSms(string inputValue)
        {
            page.activateAddMobileServiceTab();
            page.EnterPhone(inputValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));
            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("SmsSent");
            var notification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(notification.Length > 0, "Mobile service SMS send failed: notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, notification, "Mobile service SMS send failed: notification is not correct"));
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidOtp))]
        public void AddMobileService_CheckInvalidOtp(string phoneValue, string otpCode)
        {
            page.activateAddMobileServiceTab();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var waitNotification = page.WaitNotification(page.GetNotificationMsg("SmsSent"));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitNotification.Length > 0, "Failed: notification SmS sent is not shown"));
            if (waitNotification.Length > 0)
            {
                page.CloseNotification();
            }

            page.EnterOtp(otpCode);
            page.ClickMobileServiceButton();

            var expectedMsg = page.GetNotificationMsg("OtpWrongFormat");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service otp entering failed: error notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Mobile service otp entering failed: error notification is not correct"));
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckLimitOtpExceeded))]
        public void AddMobileService_CheckLimitOtpExceeded(string phoneValue, int limitOtpSending)
        {
            page.activateAddMobileServiceTab();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            for (int i = 0; i < limitOtpSending; i++)
            {
                page.ClickLinkSmsSend();
                page.isPageLoaded();
            }

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            var expectedMsg = page.GetNotificationMsg("LimitOtpCode");
            var waitErrorNotification = page.WaitNotification(expectedMsg);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service sms sending failed: error notification is not shown"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                "Mobile service sms sending failed: error notification is not correct"));
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidMobile))]
        public void AddMobileService_CheckInvalidMobileService(string phoneValue, string otpCode, string msg)
        {
            page.activateAddMobileServiceTab();
            page.EnterPhone(phoneValue);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(page.isLinkSmsEnabled(), "Phone input failed: the link SMS send doesn't get enabled"));

            page.ClickLinkSmsSend();
            page.isPageLoaded();

            page.EnterOtp(otpCode);
            var buttonEnabled = page.isMobileServiceButtonEnabled();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(buttonEnabled, "Mobile service inputs failed: button is not enabled"));

            if (buttonEnabled)
            {
                page.ClickMobileServiceButton();
                page.isPageLoaded();

                var expectedMsg = page.GetNotificationMsg(msg);
                var waitErrorNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Mobile service failed: error notification is not shown"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification,
                        "Mobile service failed: error notification is not correct"));
            }
        }

        [Test, TestCaseSource(typeof(RegistrationData), nameof(RegistrationData.CheckInvalidFixed))]
        public void AddFixedService_CheckInvalidFixedService(string subscriberCode, string referenceNumber, string msg)
        {
            page.activateAddFixedServiceTab();
            page.EnterSubscription(subscriberCode);
            page.EnterNumber(referenceNumber);
            var buttonEnabled = page.isFixedServiceButtonEnabled();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(buttonEnabled, "Fixed service inputs failed: button is not enabled"));

            if (buttonEnabled)
            {
                page.ClickFixedServiceButton();
                page.isPageLoaded();

                var expectedMsg = page.GetNotificationMsg(msg);
                var waitErrorNotification = page.WaitNotification(expectedMsg);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(waitErrorNotification.Length > 0, "Fixed service inputs failed: error notification is not shown"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedMsg, waitErrorNotification, "Fixed service inputs failed: error notification is not correct"));
            }
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
