﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestPages
{
    [TestFixture]
    [Category("TestPages")]
    class EmptyService : TestSet
    {
        LoginPage loginPage;
        EmptyServicePage page;
        private string testUser;
        private string testPassword;

        public EmptyService()
        {
            testUser = SpecificData.EmptyUser[0, 0];
            testPassword = SpecificData.EmptyUser[0, 1];
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(testUser, testPassword), string.Format("User {0} is not logged in. Test is interrupted", testUser));
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.EmptyServiceUrl),
                string.Format("Accounts page is not opened, expected {0}, but is {1}", PageUrls.EmptyServiceUrl, driver.Url));

            page = new EmptyServicePage(driver);

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void EmptyService_CheckTextsElementsOnPage()
        {
            _assertsAccumulator.Accumulate(() => Assert.That(page.isLogoDisplayed(), "Logo isn't shown"));
            _assertsAccumulator.Accumulate(() => Assert.That(page.isFooterDisplayed(), "Footer isn't shown on the page"));
            page.CheckLabels(_assertsAccumulator);
        }

        [Test]
        public void EmptyService_ShouldOpenAddService()
        {
            page.ClickAddServiceButton();
            page.isPageLoaded();

            var profileAddServicePage = new ProfileAddServicePage(driver);

            var addServiceOpened = profileAddServicePage.isAddServiceOpened();
            _assertsAccumulator.Accumulate(() => Assert.That(addServiceOpened, "Failed: AddService popup is not opened"));
            if (addServiceOpened)
            {
                profileAddServicePage.ClickMobileCancelButton();
            }
        }

        [OneTimeTearDown]
        public void logout()
        {
            page.LogOut();
        }
    }
}
