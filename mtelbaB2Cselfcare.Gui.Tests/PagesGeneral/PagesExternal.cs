﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PagesExternal : Pages
    {
        public PagesExternal(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'notification')]")]
        protected IWebElement Notification;

        public string WaitNotification(string infoText)
        {
            string newMessage = string.Empty;
            string message = newMessage;
            for (int i = 0; i < 2; i++)
            {
                newMessage = TestsHelper.WaitNotification(Notification, infoText);
                if (newMessage.Equals(infoText))
                {
                    return newMessage;
                }
                if (newMessage.Length > 0)
                {
                    message = newMessage;
                }
            }
            return message;
        }
    }
}
