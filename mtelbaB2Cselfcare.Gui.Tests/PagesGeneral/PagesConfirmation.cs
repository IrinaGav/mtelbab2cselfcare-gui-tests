﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PagesConfirmation
    {
        public PagesConfirmation(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        private IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-logo')]")]
        [CacheLookup]
        protected IWebElement Logo;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-email-confirmation']")]
        [CacheLookup]
        protected IWebElement EmailIcon;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-circled-greenmark']")]
        [CacheLookup]
        protected IWebElement SuccessIcon;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-link-expired']")]
        [CacheLookup]
        protected IWebElement LinkExpiredIcon;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-title']")]
        [CacheLookup]
        protected IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-description']")]
        [CacheLookup]
        protected IWebElement Info;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn']")]
        [CacheLookup]
        protected IWebElement Button;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'notification')]")]
        protected IWebElement Notification;

        public void OpenPage(string pageUrl)
        {
            TestsHelper.OpenPage(driver, pageUrl);
        }

        public string GetNotificationText()
        {
            return TestsHelper.GetWebText(Notification);
        }

        public string WaitNotification(string infoText)
        {
            return TestsHelper.WaitWebText(Notification, infoText);
        }

        public bool isLogoDisplayed()
        {
            return TestsHelper.isIconDisplayed(Logo);
        }

        public bool isSuccessIconDisplayed()
        {
            return TestsHelper.isIconDisplayed(SuccessIcon);
        }

        public bool isEmailIconDisplayed()
        {
            return TestsHelper.isIconDisplayed(EmailIcon);
        }

        public bool isLinkExpiredIconDisplayed()
        {
            return TestsHelper.isIconDisplayed(LinkExpiredIcon);
        }

        public bool isPageOpened(string pageUrl)
        {
            return TestsHelper.isPageOpenedUrlStartedWith(pageUrl, driver);
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator, string textTitle, string[] textInfo, string textButton)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(textTitle, Title.Text, "Wrong text for title"));
            foreach (var item in textInfo)
            {
                _assertsAccumulator.Accumulate(() => Assert.That(Info.Text.Contains(item),
                    string.Format("Wrong text for info: doesn't contain {0}", item)));
            }
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(textButton, Button.Text, "Wrong text for the button"));
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator, string textTitle, string textInfo, string textButton)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(textTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(textInfo, Info.Text, "Wrong text for info"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(textButton, Button.Text, "Wrong text for the button"));
        }

        public void ClickButton()
        {
            TestsHelper.ClickWeb(Button);
        }
    }
}
