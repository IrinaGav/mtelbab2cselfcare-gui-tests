﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PagesInner : Pages
    {
        public PagesInner(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.Id, Using = "footer-container")]
        protected IWebElement Footer;

        [FindsBy(How = How.XPath, Using = "//*[@class='notification__inner']")]
        protected IWebElement Notification;

        [FindsBy(How = How.XPath, Using = "//*[@class='user-navigation__dropdown-item' and contains(text(),'Odjavite se')]")]
        protected IWebElement ProfileMenuLogOut;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'user-navigation__dropdown') and contains(text(),'Moj profil')]")]
        protected IWebElement ProfileMenuMyProfile;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-plus sub-nav-menu__desktop-button-icon']")]
        protected IWebElement AddServiceButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__check']")]
        protected IWebElement AddServiceForm;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']")]
        protected IWebElement Popup;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal__icon-close')]")]
        protected IWebElement PopupCloseButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//div[contains(@class,'info-component__text-title')]")]
        protected IWebElement PopupTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//div[contains(@class,'info-component__text-description')]")]
        protected IWebElement PopupInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[@class='btn']")]
        protected IWebElement PopupButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[contains(@class,'btn btn--white')]")]
        protected IWebElement PopupButtonCancel;

        protected string TextButtonCancel = "Otkažite";

        [FindsBy(How = How.XPath, Using = "//*[@class='first-login']")]
        private IWebElement FirstLoginPopup;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn btn--outlined first-login__btn-cancel']")]
        private IWebElement FirstLoginPopupGotoPregledButton;

        public bool isFirstLoginPopupDisplayed()
        {
            return TestsHelper.isWebDisplayed(FirstLoginPopup);
        }

        public void CloseFirstLoginPopup()
        {
            TestsHelper.ClickWeb(FirstLoginPopupGotoPregledButton);
        }

        public bool isFooterDisplayed()
        {
            return TestsHelper.isWebDisplayed(Footer) & TestsHelper.GetWebText(Footer).Length > 0;
        }

        public string WaitNotification(string infoText)
        {
            string newMessage = string.Empty;
            string message = newMessage;
            for (int i = 0; i < 2; i++)
            {
                newMessage = TestsHelper.WaitNotification(Notification, infoText);
                if (newMessage.Equals(infoText))
                {
                    return newMessage;
                }
                if (newMessage.Length > 0)
                {
                    message = newMessage;
                }
            }
            return message;
        }

        public void LogOut()
        {
            this.isPageLoaded();
            TestsHelper.ClickWeb(IconProfile);
            TestsHelper.ClickWeb(ProfileMenuLogOut);
        }

        public void OpenAddService()
        {
            TestsHelper.ClickWeb(AddServiceButton);
        }

        public bool isPopupDisplayed()
        {
            return TestsHelper.isWebDisplayed(Popup) & TestsHelper.GetWebText(PopupTitle).Length > 0;
        }

        public void ClickCloseButtonPopup()
        {
            TestsHelper.ClickWeb(PopupCloseButton);
        }

        public void ClickPopupButton()
        {
            TestsHelper.ClickWeb(PopupButton);
        }

        public void ClickPopupButtonCancel()
        {
            TestsHelper.ClickWeb(PopupButtonCancel);
        }

        public void OpenProfileViaMenu()
        {
            TestsHelper.ClickWeb(IconProfile);
            TestsHelper.ClickWeb(ProfileMenuMyProfile);
            isPageLoaded();
        }
    }
}
