﻿namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PageMessages
    {
        internal static string Login_WrongLogin = "Unijeli ste pogrešnu e-mail adresu ili lozinku.";

        internal static string ForgottenPassword_NotExist = "Korisnik nije pronađen.";

        internal static string PasswordConfirmation_EmaiSent = "Vaš zahtjev je prihvaćen.\r\nPoslali smo vam email na {0} sa linkom za promjenu lozinke.";

        internal static string PasswordsNotMatch = "Lozinke se ne poklapaju.";
        internal static string PasswordIsOld = "Nova lozinka mora biti različita od poslednjih 5.";
        internal static string PasswordLess8 = "Lozinka mora imati najmanje 8 karaktera.";
        internal static string PasswordMore64 = "Lozinka mora imati najviše 64 karaktera.";
        internal static string PasswordNotAllowedSigns = "Lozinka sadrži jedan ili više nedozvoljenih karaktera.";
        internal static string PasswordOneTypeSigns = "Potrebno je ispuniti bar dva uslova.";
        internal static string InvalidOldPassword = "Unijeli ste pogrešnu staru lozinku.";
        internal static string UserNameInPassword = "Lozinka ne sme da sadrži Vaše korisničko ime.";

        internal static string InvalidPassword = "Unijeli ste pogrešnu lozinku.";
        internal static string PasswordIsChanged = "Vaš zahtjev je prihvaćen.\r\nLozinka je uspješno promjenjena.";

        internal static string RegistrationConfirmation_NoToken = "token missing";
        internal static string RegistrationConfirmation_EmailSent = "Vaš zahtjev je prihvaćen.\r\nPoslali smo vam email na {0} sa linkom za aktivaciju naloga.";

        internal static string Registration_MtbCustomerNotFoundError = "Podaci nisu dobro uneseni ili pripadaju računu starijem od 3 mjeseca.";
        internal static string Registration_notB2Ccustomer = "Usluga koju pokušavate da registrujete je za poslovne korisnike. Idite na biz.mtel.ba .";
        internal static string Registration_OldAccount = "Podaci nisu dobro uneseni ili pripadaju računu starijem od 3 mjeseca.";
        internal static string Registration_SmsSent = "Vaš zahtjev je prihvaćen.\r\nSMS kod je uspješno poslan.";
        internal static string Registration_OtpWrongFormat = "SMS kod nije validan.";
        internal static string Registration_LimitOtpCode = "Iskoristili ste maksimalan broj slanja u roku od jednog sata. Pokušajte kasnije.";
        internal static string Registration_OldOtpCode = "SMS kod je istekao. Pošaljite zahtjev za novi kod.";
        internal static string Registration_notB2Cphone = "Broj sa kojim ste pokušali registraciju pripada poslovnim korisnicima. Posjetite biz.mtel.ba.";
        internal static string Registration_PhoneNotFound = "Korisnik nije pronađen u m:tel sistemu.";

        internal static string AddFixedService_NotB2C = "Usluga koju ste pokušali uvezati pripada poslovnim korisnicima. Posjetite biz.mtel.ba.";
        internal static string AddService_AlreadyIsConnected = "Usluga je već uvezana na datom nalogu.";
        internal static string AddService_IsMain = "Nije moguće ukloniti glavni Moj m:tel nalog.";
        internal static string AddedService_Unlinked = "Nalog je uspješno uklonjen.";
        internal static string AddedService_Linked = "Vaš zahtjev je prihvaćen.\r\nUspješno ste dodali novu uslugu.";

        internal static string EmailChangeConfirmation_EmailSent = "Vaš zahtjev je prihvaćen.\r\nPoslali smo vam email na {0} sa linkom za promjenu e-mail adrese.";

        internal static string Msg_FieldMandatory = "Ovo polje je obavezno.";
        internal static string Msg_InvalidEmail = "Unesite e-mail u ispravnom formatu.";
        internal static string Msg_InvalidPhone = "Format broja mora biti 387xxxxxxxxx ili 0xxxxxxxx.";
        internal static string Msg_EmailExists = "Već postoji nalog na ovoj e-mail adresi.";  // "Korisnik sa ovim emial-om već postoji.";

        internal static string TariffPlanActivated = "Vaš zahtjev je prihvaćen.\r\nUspješno ste aktivirali tarifni plan {0}.";
        internal static string TariffPlanDeactivated = "Vaš zahtjev je prihvaćen.\r\nUspješno ste deaktivirali tarifni plan {0}.";
    }
}