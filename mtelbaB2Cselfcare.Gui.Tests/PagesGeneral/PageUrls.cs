﻿using OpenQA.Selenium;
using System.Configuration;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PageUrls
    {
        public static string BaseURL = ConfigurationManager.AppSettings.Get("baseUrl");

        public static string LoginUrl = getFullUrl("/Privatni/Prijava");

        public static string PregledUrl = getFullUrl("/Privatni/Prepaid/Pregled");
        public static string EmptyServiceUrl = getFullUrl("/Privatni/Dodajte-uslugu");

        public static string RegistrationUrl = getFullUrl("/Privatni/Registracija");
        public static string RegistrationConfirmationUrl = getFullUrl("/Privatni/Potvrda-registracije");
        public static string RegistrationLinkExpiredUrl = getFullUrl("/Privatni/Istekao-verifikacioni-link");

        public static string ForgottenPasswordUrl = getFullUrl("/Privatni/Zaboravljena-lozinka");
        public static string PasswordConfirmationUrl = getFullUrl("/Privatni/Zaboravljena-lozinka/Potvrda-lozinke");
        public static string NewPasswordUrl = getFullUrl("/Privatni/Zaboravljena-lozinka/Nova-lozinka");
        public static string PasswordChangedUrl = getFullUrl("/Privatni/Promijenjena-lozinka");
        public static string ForgottenPasswordLinkExpiredUrl = getFullUrl("/Privatni/Zaboravljena-lozinka/Istekao-link");

        public static string AccountsUrl = getFullUrl("/Privatni/Uvezani-nalozi");

        public static string ProfileChangeEmailUrl = getFullUrl("/Privatni/Promjena-email-adrese");
        public static string ProfileChangePasswordUrl = getFullUrl("/Privatni/Promjena-lozinke");
        public static string ProfileDeleteAccountUrl = getFullUrl("/Privatni/Brisanje-naloga");

        public static string PrepaidMobileOverviewUrl = getFullUrl("/Privatni/Prepaid/Pregled?subscriberIdentity={0}");
        public static string PrepaidMobileChangeTariffUrl = getFullUrl("/Privatni/Prepaid/Promjena-tarife?subscriberIdentity={0}");

        public static string PostpaidMobileOverviewUrl = getFullUrl("/Privatni/Postpaid/Pregled?subscriberIdentity={0}");
        public static string PostpaidMobileDopunaUrl = getFullUrl("/Privatni/Postpaid/Online-dopuna?subscriberIdentity={0}");

        private static string getFullUrl(string url)
        {
            return BaseURL + url;
        }
    }
}
