﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class Pages
    {
        public Pages(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        private IWebDriver driver;

        protected const string labelPrefix = "//*[@class='form-field__label' and @for=";
        protected const string inputPrefix = "//*/input[@id=";

        public string PageLoader = "//*[@class='loader__wrapper']";

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-logo')]")]
        [CacheLookup]
        protected IWebElement Logo;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-exclamation-sign']")]
        protected IWebElement IconExclamation;

        [FindsBy(How = How.XPath, Using = "//*/i[contains(@class,'user-navigation__icon icon-profile')]")]
        protected IWebElement IconProfile;

        [FindsBy(How = How.XPath, Using = "//*[@class='form-field__error']")]
        protected IList<IWebElement> MsgError;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-close notification')]")]
        protected IWebElement IconCloseNotification;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-cross')]")]
        protected IWebElement IconErrorNotification;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-checkmark')]")]
        protected IWebElement IconSuccessNotification;

        [FindsBy(How = How.XPath, Using = "//*[@class='main-layout__title']")]
        [CacheLookup]
        protected IWebElement Title;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'email']")]
        [CacheLookup]
        protected IWebElement LabelEmail;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'email']")]
        [CacheLookup]
        protected IWebElement InputEmail;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'password']")]
        [CacheLookup]
        protected IWebElement LabelPassword;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'password']")]
        protected IWebElement InputPassword;

        [FindsBy(How = How.XPath, Using = "//*/button[contains(@class,'btn')]")]
        protected IWebElement Button;

        internal string TextLabelEmail = "E-mail adresa";
        internal string TextLabelYourPassword = "Vaša lozinka";
        internal string TextLabelPassword = "Lozinka";
        internal string TextLinkForgotPassword = "Zaboravili ste lozinku?";

        public bool isLogoDisplayed()
        {
            return TestsHelper.isIconDisplayed(Logo);
        }

        public bool isPageLoaded()
        {
            return TestsHelper.isPageLoaded(driver, PageLoader);
        }

        public bool isProfileDisplayed()
        {
            return TestsHelper.isWebDisplayed(IconProfile);
        }

        public void CloseNotification()
        {
            TestsHelper.ClickWeb(IconCloseNotification);
        }

        public void EnterEmail(string inputValue)
        {
            TestsHelper.EnterValue(InputEmail, inputValue);
        }

        public void EnterPassword(string inputValue)
        {
            Helper.EnterValue(InputPassword, inputValue);
        }

        public bool isButtonDisabled()
        {
            return TestsHelper.isWebDisabled(Button);
        }

        public bool isButtonEnabled()
        {
            return TestsHelper.isWebEnabled(Button);
        }

        public void ClickButton()
        {
            TestsHelper.ClickWeb(Button);
        }
    }
}
