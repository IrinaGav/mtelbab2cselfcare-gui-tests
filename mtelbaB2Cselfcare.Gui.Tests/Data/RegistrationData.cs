﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    public class RegistrationData
    {
        public static IEnumerable<TestCaseData> CheckValidInputPhone()
        {
            return DataHelper.getFeatureUserData1(CommonData.ValidPhone);
        }

        public static IEnumerable<TestCaseData> CheckInvalidInputPhone()
        {
            return DataHelper.getFeatureUserData1(CommonData.InvalidPhone);
        }

        public static IEnumerable<TestCaseData> CheckInvalidOtp()
        {
            return DataHelper.getFeatureUserData2(CommonData.InvalidOtpCode);
        }

        public static IEnumerable<TestCaseData> CheckLimitOtpExceeded()
        {
                yield return new TestCaseData("387628581869", CommonData.LimitOtpCodeSending);
        }

        public static IEnumerable<TestCaseData> CheckInvalidMobile()
        {
            return DataHelper.getFeatureUserData3(SpecificData.RegistrationMobileWrongData);
        }

        public static IEnumerable<TestCaseData> CheckMobileAccountExistMain()
        {
            return DataHelper.getFeatureUserData4(SpecificData.RegistrationMobileExistMainData);
        }

        public static IEnumerable<TestCaseData> CheckMobileAccountExistNotMain()
        {
            return DataHelper.getFeatureUserData4(SpecificData.RegistrationMobileExistNotMainData);
        }

        public static IEnumerable<TestCaseData> CheckInvalidFixed()
        {
            return DataHelper.getFeatureUserData3(SpecificData.RegistrationFixedWrongData);
        }

        public static IEnumerable<TestCaseData> CheckFixedNewAccount()
        {
            return DataHelper.getFeatureUserData2(SpecificData.RegistrationFixedNotRegisteredData);
        }
    }
}
