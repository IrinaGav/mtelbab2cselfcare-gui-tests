﻿using mtelbaB2Cselfcare.Gui.Tests.Pages;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class SpecificDataDev
    {
        internal static string[,] TestUsers = {
            { "dummyuser@dummy.com", CommonData.CommonPassword, PageUrls.PregledUrl },
            { "dummyuser1@dummy.com", CommonData.CommonPassword, PageUrls.PregledUrl },
            { "dummyuser3@dummy.com", CommonData.CommonPassword, PageUrls.EmptyServiceUrl},
        };

        internal static string[,] GeneralAutoUser = { { "auto.test@dummy.com", CommonData.CommonPassword, "AutoTest1 AutoTest1" } };
        internal static string[,] GeneralTestUser = { { "dummyuser@dummy.com", CommonData.CommonPassword, "Gradimir Ljubić" } };
        internal static string[,] EmptyUser = { { "dummyuser3@dummy.com", CommonData.CommonPassword } };

        internal static string[,] RegistrationFixedWrongData = {
            { "123456", "123456", "MtbCustomerNotFoundError" },
            { "5802106663", "1446070802", "notB2Ccustomer" },
            { "5802106662", "1446070801", "OldAccount" },
            { "5802106661", "1446070800", "OldAccount" }};

        internal static string[,] RegistrationFixedNotRegisteredData = { { "5802106664", "1446070803 " } };

        internal static string[,] RegistrationMobileWrongData = {
            { "38761138949", CommonData.CommonOtp, "notB2Cphone" },
            { "38761188999", CommonData.CommonOtp, "PhoneNotFound" } };

        internal static string[,] RegistrationMobileExistMainData = { { "38765480083", CommonData.CommonOtp, "dummyuser1@dummy.com", CommonData.CommonPassword } };

        internal static string[,] RegistrationMobileExistNotMainData = { { "38765685936", CommonData.CommonOtp, "dummyuser1@dummy.com", CommonData.CommonPassword } };

        internal static string[,] RegistrationMobileNotRegisteredData = { { "38766000005", CommonData.CommonOtp } };

        internal static string[,] AddServiceMobileIsConnected = {
            { "38766176726", CommonData.CommonOtp, "dummyuser@dummy.com", CommonData.CommonPassword } };

        internal static string[,] AddServiceMobileExistMainData = {
            { "dummyuser@dummy.com", CommonData.CommonPassword, "38765480083", CommonData.CommonOtp, "dummyuser1@dummy.com", CommonData.CommonPassword } };

        internal static string[,] AddServiceMobile = {
            { "dummyuser1@dummy.com", CommonData.CommonPassword, "38766646870", CommonData.CommonOtp } };

        internal static string[,] ProfileUser = { { "dummyuser@dummy.com", CommonData.CommonPassword } };

        internal static string[,] ProfileChangePasswordUser = { { "auto.test4@dummy.com", CommonData.CommonPassword } };

        internal static string[,] ProfileChangePasswordUserInvalidInput = { { "auto.test3@dummy.com", CommonData.CommonPassword } };

        internal static string[,] ProfileChangePasswordUserFlow = { { "auto.test5@dummy.com", CommonData.CommonPassword } };

        internal static string[] ProfileChangeEmailExistentEmails = { "auto.test3@dummy.com", "auto.test4@dummy.com" };

        internal static string[,] PrepaidMobileChangeTariffUser = { { "dummyuser@dummy.com", CommonData.CommonPassword, "38765587814" } };

        internal static string[,] PrepaidMobileResourcesUser = { { "dummyuser@dummy.com", CommonData.CommonPassword, "38765587814" } };

        internal static string[,] PrepaidMobileGraceUserWithoutCredit = { { "dummyuser@dummy.com", CommonData.CommonPassword, "38765483601" } };

        internal static string[,] PrepaidMobileGraceUserActive3Days = { { "dummyuser@dummy.com", CommonData.CommonPassword, "38766003996" } };

        internal static string[,] PostpaidMobileResourcesUser = { { "dummyuser@dummy.com", CommonData.CommonPassword, "38766906330" } };
    }

    class PrepaidMobileRersoucesTestData
    {
        public string[] BonusesHeader;
        public string[] BonusName;
        public string[] BonusExpiryDate;
        public int BonusesProgressBarRed;
        public int BonusesProgressBarBlue;
        public string[] OptionExpiryDate;
        public string[] BonusesResourceValue;
        public string[] BonusesAbsValue;
        public string[] TariffPlanResourceName;
        public string[] TariffPlanResourceValue;
        public string[] TariffPlanResourceUnlim;
        public string TariffPlanNote;

        public PrepaidMobileRersoucesTestData(string serviceId)
        {
            BonusesHeader = new string[] { "VAŠI BONUSI", "BONUSI UKLJUČENI U TARIFNE OPCIJE" };
            if (serviceId.Equals("38765587814"))
            {
                BonusName = new string[] { "Bonus internet", "Bonus SMS", "Bonus minute", "Roming NET Slovenija",
                    "Internet (u regionu Zapadnog Balkana), tarifni plan: Dopuna:XY promo", "Preostale minute" };
                BonusExpiryDate = new string[] { "Važi do - 05.07.2021.", "Važi do - 30.06.2021.", "Važi do - 12.06.2021." };
                BonusesProgressBarRed = 3;
                BonusesProgressBarBlue = 1;
                OptionExpiryDate = new string[] {"* Tarifna opcija važi do 21.06.2021.", "* Tarifna opcija važi do 13.06.2021."};
                BonusesResourceValue = new string[] { "646 MB /\r\n2048 MB", "114 MB /\r\n114 MB" };
                BonusesAbsValue = new string[] { "100\r\nSMS", "175\r\nMIN", "2048\r\nMB", "300\r\nMIN" };
                TariffPlanResourceName = new string[] { "Preostali internet", "Facebook" };
                TariffPlanResourceValue = new string[] { "4096 MB /\r\n4096 MB" };
                TariffPlanResourceUnlim = new string[] { "NEOGRANIČENO" };
                TariffPlanNote = "* Prikaz resursa je za saobraćaj ostvaren od trenutka aktivacije/obnove tarifnog plana i važi 30 dana.";
            }
            if (serviceId.Equals("38766176726"))
            {
                BonusName = new string[] { "Bonus internet", "Bonus SMS", "Bonus minute", "Roming NET Slovenija", "Preostale minute" };
                BonusExpiryDate = new string[] { "Važi do - 05.07.2021.", "Važi do - 30.06.2021.", "Važi do - 12.06.2021." };
                BonusesProgressBarRed = 2;
                BonusesProgressBarBlue = 1;
                OptionExpiryDate = new string[] { "* Tarifna opcija važi do 21.06.2021.", "* Tarifna opcija važi do 13.06.2021." };
                BonusesResourceValue = new string[] { "646 MB /\r\n2048 MB" };
                BonusesAbsValue = new string[] { "100\r\nSMS", "175\r\nMIN", "2048\r\nMB", "300\r\nMIN" };
                TariffPlanResourceName = new string[] { "Preostali internet", "Facebook" };
                TariffPlanResourceValue = new string[] { "4096 MB /\r\n4096 MB" };
                TariffPlanResourceUnlim = new string[] { "NEOGRANIČENO" };
                TariffPlanNote = "* Prikaz resursa je za saobraćaj ostvaren od trenutka aktivacije/obnove tarifnog plana i važi 30 dana.";
            }
        }
    }

    class PostpaidMobileRersoucesTestData
    {
        public string ResourcesHeader = "BONUSI UKLJUČENI U PRETPLATU";
        public string TariffOpionsHeader = "BONUSI UKLJUČENI U TARIFNE OPCIJE";
        public string[] TariffResourceNames;
        public string[] TariffOptionsNames;
        public string[] BonusExpiryDate;
        public int BonusesProgressBarRed;
        public int BonusesProgressBarBlue;
        public string[] OptionExpiryDate;
        public string[] TariffResourceValue;
        public string[] TariffOptionsValue;
        public string AddonsTableName;
        public string[] TableColumnName;
        public string[] TableColumnValue;
        public string TariffPlanNote;
        public bool IsBannerShown;
        public bool IsInstallmentsShown;


        public PostpaidMobileRersoucesTestData(string serviceId)
        {
            if (serviceId.Equals("38766906330"))
            {
                TariffResourceNames = new string[] {
                    "Bonus internet ukljucen u pretplatu u m:tel mrezi i u regionu Zapadnog Balkana",
                    "Bonus internet ukljucen u pretplatu u regionu Zapadnog Balkana",
                    "Minuti ZONA3",
                    "Minuti AUSTRIA",
                    "Minuti",
                    "Minuti MTS",
                    "SMS poruke" };
                TariffOptionsNames = new string[] {
                    "4 GB / 30 dana (u regionu Zapadnog Balkana)",
                    "4 GB / 30 dana (u BiH)",
                    "20 GB / 30 dana (u regionu Zapadnog Balkana)",
                    "20 GB / 30 dana (u BiH)",
                    "Usluga SiguranNet"
                };
                BonusesProgressBarRed = 10;
                BonusesProgressBarBlue = 2;
                TariffResourceValue = new string[] {
                    "2048 MB /\r\n2048 MB", "1803 MB /\r\n1803 MB", "50 MIN /\r\n50 MIN", "50 MIN /\r\n50 MIN", "50 MIN /\r\n50 MIN"};
                TariffPlanNote = "* Prikaz bonusa uključenih u pretplatu je za tekući mjesec. Bonus internet se obnavlja svakog prvog dana u mjesecu.";
                TariffOptionsValue = new string[] { "1992 MB /\r\n1992 MB",  "4096 MB /\r\n4096 MB", "2270 MB /\r\n2270 MB", "20480 MB /\r\n20480 MB", "100 MB /\r\n100 MB",
                };
                AddonsTableName = "AKTIVIRANI DODACI";
                TableColumnName = new string[] { "Naziv", "Cena", "Datum aktivacije", "Datum obnove", };
                TableColumnValue = new string[] { "SiguranNet PROMO", "1,17 KM", "05.06.2021.", "03.09.2021." };
                IsBannerShown = true;
                IsInstallmentsShown = false;
            }
            if (serviceId.Equals("38766309363"))
            {
                TariffResourceNames = new string[] { };
                BonusesProgressBarRed = 0;
                BonusesProgressBarBlue = 0;
                TariffResourceValue = new string[] { };
                TariffPlanNote = null;
                AddonsTableName = "AKTIVIRANI DODACI";
                TableColumnName = new string[] { "Naziv", "Cena", "Datum aktivacije", "Datum obnove", };
                TableColumnValue = new string[] { "SiguranNet PROMO", "1,17 KM", "05.06.2021.", "03.09.2021." };
                IsBannerShown = false;
                IsInstallmentsShown = true;
            }
        }
    }
}
