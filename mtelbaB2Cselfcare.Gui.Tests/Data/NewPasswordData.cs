﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class NewPasswordData
    {
        public static IEnumerable<TestCaseData> CheckEnteringPassword()
        {
            int index = Helper.GetRandomIndex(0, 3);
            string[] passwordCharacters = { "asdf", "ASDF", "1234" };

            yield return new TestCaseData(passwordCharacters[0] + passwordCharacters[1], true);
            yield return new TestCaseData(passwordCharacters[0] + passwordCharacters[2], true);
            yield return new TestCaseData(passwordCharacters[1] + passwordCharacters[2], true);
            yield return new TestCaseData(passwordCharacters[index] + CommonData.SpecialSigns, true);
            yield return new TestCaseData(CommonData.SpecialSigns, false);
            foreach (var item in passwordCharacters)
            {
                yield return new TestCaseData(item + item, false);
            }

            string tooLongPassword = "9a";
            for (int i = 0; i < 16; i++)
            {
                tooLongPassword = tooLongPassword + passwordCharacters[index];
            }
            yield return new TestCaseData(tooLongPassword, false);
        }

        public static IEnumerable<TestCaseData> CheckInvalidEnteringPassword()
        {
            yield return new TestCaseData("1234", true);
        }

        public static IEnumerable<TestCaseData> CheckInvalidEnteringPasswordConfirmation()
        {
            yield return new TestCaseData("1234asdf");
        }
    }
}
