﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class ProfileChangeEmailData
    {
        internal static IEnumerable<TestCaseData> CheckInvalidEmail()
        {
            var userCreds = SpecificData.ProfileUser;
            yield return new TestCaseData(userCreds[0, 0], userCreds[0, 1], CommonData.InvalidEmail);
        }

        internal static IEnumerable<TestCaseData> CheckEmailExists()
        {
            var userCreds = SpecificData.ProfileUser;
            yield return new TestCaseData(userCreds[0, 0], userCreds[0, 1], SpecificData.ProfileChangeEmailExistentEmails);
        }
    }
}
