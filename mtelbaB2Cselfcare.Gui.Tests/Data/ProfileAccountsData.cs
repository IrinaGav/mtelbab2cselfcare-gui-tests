﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class ProfileAccountsData
    {
        public static IEnumerable<TestCaseData> CheckMobileAccountExistMain()
        {
            return DataHelper.getFeatureUserData4(SpecificData.RegistrationMobileExistMainData);
        }

        public static IEnumerable<TestCaseData> CheckMobileAccountIsConnected()
        {
            return DataHelper.getFeatureUserData2(SpecificData.AddServiceMobileIsConnected);
        }

        public static IEnumerable<TestCaseData> CheckMobileExistNotMainData()
        {
            return DataHelper.getFeatureUserData4(SpecificData.RegistrationMobileExistNotMainData);
        }

        public static IEnumerable<TestCaseData> CheckMobileExistMainData()
        {
            return DataHelper.getFeatureUserData6(SpecificData.AddServiceMobileExistMainData);
        }

        public static IEnumerable<TestCaseData> CheckAddMobileService()
        {
            return DataHelper.getFeatureUserData4(SpecificData.AddServiceMobile);
        }
    }
}
