﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class ProfileChangePasswordData
    {
        internal static IEnumerable<TestCaseData> CheckProfileChangePasswordFlowUser()
        {
            return DataHelper.getFeatureUserData2(SpecificData.ProfileChangePasswordUserFlow);
        }

        internal static IEnumerable<TestCaseData> CheckProfileChangePasswordUser()
        {
            return DataHelper.getFeatureUserData2(SpecificData.ProfileChangePasswordUser);
        }

        internal static IEnumerable<TestCaseData> CheckInvalidNewPassword()
        {
            var userCreds = SpecificData.ProfileChangePasswordUserInvalidInput;
            yield return new TestCaseData(userCreds[0, 0], userCreds[0, 1], CommonData.InvalidPassword);
        }

        internal static IEnumerable<TestCaseData> CheckValidNewPassword()
        {
            var userCreds = SpecificData.ProfileChangePasswordUserInvalidInput;
            string[] passwordCharacters = { "asdf", "ASDF", "1234" };

            string[] newPassword = new string[6];
            newPassword[0] = passwordCharacters[0] + passwordCharacters[1];
            newPassword[1] = passwordCharacters[0] + passwordCharacters[2];
            newPassword[2] = passwordCharacters[1] + passwordCharacters[2];
            newPassword[3] = passwordCharacters[0] + CommonData.SpecialSigns;
            newPassword[5] = CommonData.SpecialSigns + passwordCharacters[1];
            newPassword[4] = passwordCharacters[2] + CommonData.SpecialSigns;

            yield return new TestCaseData(userCreds[0, 0], userCreds[0, 1], newPassword);
        }

        internal static IEnumerable<TestCaseData> CheckProfileUserInputs()
        {
            return DataHelper.getFeatureUserData2(SpecificData.ProfileChangePasswordUserInvalidInput);
        }
    }
}
