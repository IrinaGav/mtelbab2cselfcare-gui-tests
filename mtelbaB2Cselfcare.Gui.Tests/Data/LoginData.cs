﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class LoginData
    {
        public static IEnumerable<TestCaseData> CheckInvalidInputEmail()
        {
            return DataHelper.getFeatureUserData1(CommonData.InvalidEmail);
        }

        public static IEnumerable<TestCaseData> CheckWrongLogin()
        {
            yield return new TestCaseData("test.wrong.email@test.com", CommonData.CommonPassword);
            yield return new TestCaseData("test.user@test.com", "111111111111");
        }

        public static IEnumerable<TestCaseData> CheckNotExistentEmail()
        {
            yield return new TestCaseData("test.wrong.email@test.com");
        }
    }
}
