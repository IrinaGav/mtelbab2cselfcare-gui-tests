﻿using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;

namespace mtelbaB2Cselfcare.Gui.Tests.Data
{
    class CommonData
    {
        internal static string newUserEmail = ConfigurationManager.AppSettings.Get("newUserEmail");
        internal static string userEmail = ConfigurationManager.AppSettings.Get("userEmail");

        internal static int LimitOtpCodeSending = 5;
        internal static int OldPasswordInMemory = 5;
        internal static string CommonPassword = "12341234A@";
        internal static string CommonOtp = "123456";

        internal static string[] ValidPhone = new string[]
        {
            "38762345678","012345678","+38712345678","0038792345678","+387-523.456 78", "0038768259259"
        };

        internal static string[] InvalidPhone = new string[]
        {
            "3871234567","38712345678901","asdww11","38812345678","3876 456 789"
        };

        internal static string[] InvalidEmail = new string[]
        {
            "test@test","test@testtest@test","test@t est","test@test.for.","t..e-st@test.com","test.test.com","test@test.1com"
        };

        internal static string[,] InvalidOtpCode = new string[,]
       {
           { "38752345918", "1234" },
           { "38764345918", "asdfgh" },
           { "38762445918", "123 123" },
           { "38762745918", "123123" }
       };

        internal static string[,] InvalidPassword = new string[,]
        {
           { "1234567", PageMessages.PasswordLess8},
           { "123456789112345678921234567893123456789412345678951234567896asdfe", PageMessages.PasswordMore64},
           { "123456780", PageMessages.PasswordOneTypeSigns},
           { "asdfghjkl", PageMessages.PasswordOneTypeSigns},
           { "ASDZXCFGHVBN", PageMessages.PasswordOneTypeSigns},
           { "@%+!#$^?:,(){}", PageMessages.PasswordOneTypeSigns},
           { "1234 ASDF", PageMessages.PasswordNotAllowedSigns}
       };

        internal static string SpecialSigns = "@%+!#$^?:,(){}[]~&=;-_<>*./";

        internal static string[,] ForgottenPasswordUser = { { "dummyuser@dummy.com", CommonPassword } };
        internal static string[,] ForgottenPasswordEmail = { { userEmail, CommonPassword } };
    }

    class SpecificData : SpecificDataDev
    {    }

    class FeatureTestData
    {
        internal static IEnumerable<TestCaseData> TestUsers()
        {
            return DataHelper.getFeatureUserData3(SpecificData.TestUsers);
        }

        internal static IEnumerable<TestCaseData> GeneralUser()
        {
            return DataHelper.getFeatureUserData2(SpecificData.GeneralAutoUser);
        }

        internal static IEnumerable<TestCaseData> CheckForgottenPasswordFlow()
        {
            return DataHelper.getFeatureUserData2(CommonData.ForgottenPasswordUser);
        }

        internal static IEnumerable<TestCaseData> CheckProfileUser()
        {
            return DataHelper.getFeatureUserData2(SpecificData.ProfileUser);
        }

        internal static IEnumerable<TestCaseData> CheckPrepaidMobileGraceUser()
        {
            return DataHelper.getFeatureUserData3(SpecificData.PrepaidMobileGraceUserWithoutCredit);
        }

        internal static IEnumerable<TestCaseData> CheckPrepaidMobileGraceActive3DaysUser()
        {
            return DataHelper.getFeatureUserData3(SpecificData.PrepaidMobileGraceUserActive3Days);
        }
    }

    class DataHelper
    {
        internal static IEnumerable<TestCaseData> getFeatureUserData1(string[] userData)
        {
            foreach (var item in userData)
            {
                yield return new TestCaseData(item);
            }
        }

        internal static IEnumerable<TestCaseData> getFeatureUserData2(string[,] userData)
        {
            for (int i = 0; i < userData.GetLength(0); i++)
            {
                yield return new TestCaseData(userData[i, 0], userData[i, 1]);
            }
        }

        internal static IEnumerable<TestCaseData> getFeatureUserData3(string[,] userData)
        {
            for (int i = 0; i < userData.GetLength(0); i++)
            {
                yield return new TestCaseData(userData[i, 0], userData[i, 1], userData[i, 2]);
            }
        }

        internal static IEnumerable<TestCaseData> getFeatureUserData4(string[,] userData)
        {
            for (int i = 0; i < userData.GetLength(0); i++)
            {
                yield return new TestCaseData(userData[i, 0], userData[i, 1], userData[i, 2], userData[i, 3]);
            }
        }

        internal static IEnumerable<TestCaseData> getFeatureUserData6(string[,] userData)
        {
            for (int i = 0; i < userData.GetLength(0); i++)
            {
                yield return new TestCaseData(userData[i, 0], userData[i, 1], userData[i, 2], userData[i, 3], userData[i, 4], userData[i, 5]);
            }
        }
    }
}

