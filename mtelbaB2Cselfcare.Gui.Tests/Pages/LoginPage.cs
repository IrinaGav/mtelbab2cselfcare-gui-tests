﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class LoginPage : PagesExternal
    {
        public LoginPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='page-login__form-title']")]
        [CacheLookup]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'email']")]
        [CacheLookup]
        private IWebElement LabelEmail;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class='icon-eye']")]
        [CacheLookup]
        private IWebElement IconEye;

        [FindsBy(How = How.XPath, Using = "//*[@type='checkbox']")]
        [CacheLookup]
        private IWebElement Checkbox;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-login__submit btn']")]
        [CacheLookup]
        private IWebElement ButtonLogin;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-login__link-field link']")]
        [CacheLookup]
        private IWebElement LinkForgotPassword;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-login__form-footer-link link']")]
        [CacheLookup]
        private IWebElement LinkRegistration;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-login__form-footer']")]
        [CacheLookup]
        private IWebElement LabelLinkRegistration; 

        [FindsBy(How = How.XPath, Using = "//*[@type='checkbox']/..")]
        [CacheLookup]
        private IWebElement LabelCheckbox;

        private string TextTitle = "Prijavite se na svoj nalog";
        private string TextLabelEmail = "E-mail";
        private string TextLabelLoginPassword = "Lozinka";
        private string TextLabelCheckbox = "Zapamti me";
        private string TextButtonLogin = "Prijavite se";
        private string TextLinkRegistration = "Nemate nalog na moj.mtel.ba? Registrujte se\r\nZa pomoć oko prijave i registracije na portal pozovite 0800 50 000.";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelEmail, LabelEmail.Text, "Wrong label for Email field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelLoginPassword, LabelPassword.Text, "Wrong text for Password field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelCheckbox, LabelCheckbox.Text, "Wrong text for Remember me Checkbox"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkForgotPassword, LinkForgotPassword.Text, "Wrong text for link to Forgot password page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkRegistration, LabelLinkRegistration.Text, "Wrong text for link to Registration page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonLogin, ButtonLogin.Text, "Wrong text for the button"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputEmail, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelEmail)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPassword, "12", MsgError[1]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPassword)));
        }

        public void EnterEmail(string inputValue)
        {
            Helper.EnterValue(InputEmail, inputValue);
        }

        public void ClickButton()
        {
            TestsHelper.ClickWeb(ButtonLogin);
        }

        public string GetMsgInputEmail()
        {
            return MsgError[0].Text;
        }

        public void ClickLinkToForgotPassword()
        {
            TestsHelper.ClickWeb(LinkForgotPassword);
        }

        public void ClickLinkToRegistration()
        {
            TestsHelper.ClickWeb(LinkRegistration);
        }

        public bool loginByUser(string user, string password)
        {
            EnterEmail(user);
            EnterPassword(password);
            ClickButton();
            isPageLoaded();
            return isProfileDisplayed();           
        }
    }
}
