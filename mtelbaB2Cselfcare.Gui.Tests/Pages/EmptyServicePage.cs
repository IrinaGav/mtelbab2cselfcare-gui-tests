﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class EmptyServicePage : PagesInner
    {
        public EmptyServicePage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-title page-account-without-customer__info-component-text-title']")]
        private IWebElement PageTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-description page-account-without-customer__info-component-text-description']")]
        private IWebElement PageInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn page-account-without-customer__button']")]
        private IWebElement PageButton;

        private string TextPageTitle = "Trenutno nemate nijednu uvezanu uslugu na nalogu.";
        private string TextPageInfo = "Kako biste nastavili korišćenje moj mtel portala, molimo dodajte uslugu.";
        private string TextPageButton = "Dodajte uslugu";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageTitle, PageTitle.Text, "Wrong text for title in the page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageInfo, PageInfo.Text, "Wrong text for info in the page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageButton, PageButton.Text, "Wrong text for button in the page"));
        }

        public void ClickAddServiceButton()
        {
            TestsHelper.ClickWeb(PageButton);
        }
    }
}
