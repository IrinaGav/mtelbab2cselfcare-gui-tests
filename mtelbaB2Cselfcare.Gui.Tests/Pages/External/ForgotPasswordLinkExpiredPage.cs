﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ForgotPasswordLinkExpiredPage : PagesConfirmation
    {
        public ForgotPasswordLinkExpiredPage(IWebDriver driver): base(driver)
        { }

        private string TextTitle = "Link za promjenu lozinke je istekao!";
        private string TextInfo = "Važenje linka je 30 minuta. Ako i dalje želite promjeniti lozinku, pokrenite ponovo proces promjene.";

        private string TextButton = "Pokrenite ponovo";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            CheckLabels(_assertsAccumulator, TextTitle, TextInfo, TextButton);
        }
    }
}
