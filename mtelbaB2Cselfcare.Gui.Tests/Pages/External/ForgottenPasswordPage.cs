﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ForgottenPasswordPage : PagesExternal
    {
        public ForgottenPasswordPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-title']")]
        [CacheLookup]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-subtitle page-forgot-password__header-subtitle']")]
        [CacheLookup]
        private IWebElement info;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__submit btn']")]
        [CacheLookup]
        private IWebElement ButtonSend;

        [FindsBy(How = How.XPath, Using = "//*[@class='back-btn registration-layout__back-btn']")]
        [CacheLookup]
        private IWebElement LinkToLogin;

        private string TextTitle = "Zaboravili ste lozinku?";
        private string TextInfo = "Unesite svoju e-mail adresu u polje i poslaćemo Vam link za promjenu zaboravljene lozinke.";
        private string TextButton = "Pošaljite";
        private string TextLinkToLogin = "Nazad na prijavu";
        private string TextMsgNotExistentEmail = "Unijeli ste pogrešnu e-mail adresu.";

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "NotExist":
                    return PageMessages.ForgottenPassword_NotExist;
            };
            return "";
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInfo, info.Text, "Wrong text for info"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelEmail, LabelEmail.Text, "Wrong label for Email field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkToLogin, LinkToLogin.Text, "Wrong text for link to Login page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButton, ButtonSend.Text, "Wrong text for the button"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputEmail, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelEmail)));
        }

        public string GetMsgInputEmail()
        {
            return MsgError[0].Text;
        }

        public void ClickLinkToLogin()
        {
            TestsHelper.ClickWeb(LinkToLogin);
        }

        public IWebElement GetButtonSend()
        {
            return ButtonSend;
        }
    }
}
