﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PasswordConfirmationPage : PagesConfirmation
    {
        public PasswordConfirmationPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='email-confirmation-container__footer']")]
        [CacheLookup]
        internal IWebElement InfoBottom;

        private string TextTitle = "E-mail potvrda";
        private string[] TextInfo = {
            "Poslali smo email na sa linkom za promjenu lozinke. Ako ga ne možete pronaći u inbox-u, pogledajte SPAM folder ili ponovo pošaljite Zahtjev."
        };
        private string TextButton = "Pokušajte ponovo";
        private string TextInfoBottom = "Link za promjenu lozinke važi 30 minuta.";

        public string GetNotificationMsg(string typeMsg, string textMsg)
        {
            switch (typeMsg)
            {
                case "EmaiSent":
                    return string.Format(PageMessages.PasswordConfirmation_EmaiSent, textMsg);
            };
            return "";
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            CheckLabels(_assertsAccumulator, TextTitle, TextInfo, TextButton);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInfoBottom, InfoBottom.Text, "Wrong text for InfoBottom"));
        }
    }
}
