﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class NewPasswordPage : PagesExternal
    {
        public NewPasswordPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-title']")]
        [CacheLookup]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-subtitle page-new-password__header-subtitle']")]
        [CacheLookup]
        private IWebElement Info;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'password_repeat']")]
        [CacheLookup]
        private IWebElement LabelPasswordRepeat;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'password_repeat']")]
        [CacheLookup]
        private IWebElement InputPasswordRepeat;

        private string IconEye = "//*[contains(@class,'icon-eye']";  // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__submit btn']")]
        [CacheLookup]
        private IWebElement SubmitButton;

        private string TextTitle = "Kreirajte novu lozinku";
        private string TextInfo = "Lozinka treba da ima najmanje 8 karaktera i ispuni najmanje 2 uslova: veliko slovo, malo slovo, broj, simbol";
        private string TextLabelNewPassword = "Nova lozinka";
        private string TextLabelPasswordRepeat = "Potvrdite lozinku";
        private string TextButton = "Sačuvajte lozinku";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for titel"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInfo, Info.Text, "Wrong text for info"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelNewPassword, LabelPassword.Text, "Wrong text for Name of password field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPasswordRepeat, LabelPasswordRepeat.Text, "Wrong text for Name of password repeat field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButton, Button.Text, "Wrong the button text"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPassword, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelYourPassword)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPasswordRepeat, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPasswordRepeat)));
        }

        public void EnterPasswordConfirmation(string inputValue)
        {
            Helper.EnterValue(InputPasswordRepeat, inputValue);
        }

        public bool isSubmitButtonEnabled()
        {
            return Helper.isWebEnabled(SubmitButton);
        }
    }
}
