﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class RegistrationPage : PagesExternal
    {
        public RegistrationPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'bottom')]/a[@class='back-btn']")]
        [CacheLookup]
        private IWebElement LinkToLogin;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-form__block-header']")]
        [CacheLookup]
        private IList<IWebElement> TitleBlockHeader;   // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-form__block-description']")]
        [CacheLookup]
        private IList<IWebElement> InfoBlockDescription;   // 2 items 

        [FindsBy(How = How.XPath, Using = labelPrefix + "'phone']")]
        [CacheLookup]
        private IWebElement LabelPhone;

        [FindsBy(How = How.XPath, Using = "//*/input[@id='phone']")]
        [CacheLookup]
        private IWebElement InputPhone;

        [FindsBy(How = How.XPath, Using = "//*[@class='send-sms-btn']")]
        [CacheLookup]
        private IWebElement LinkSmsSend;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'sms_code']")]
        [CacheLookup]
        private IWebElement LabelSmsCode;

        [FindsBy(How = How.XPath, Using = "//*/input[@id='sms_code']")]
        private IWebElement InputSmsCode;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'subscriber_code']")]
        [CacheLookup]
        private IWebElement LabelSubscription;

        [FindsBy(How = How.XPath, Using = "//*/input[@id='subscriber_code']")]
        [CacheLookup]
        private IWebElement InputSubscription;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'reference_number']")]
        [CacheLookup]
        private IWebElement LabelNumber;

        [FindsBy(How = How.XPath, Using = "//*/input[@id='reference_number']")]
        [CacheLookup]
        private IWebElement InputNumber;

        [FindsBy(How = How.XPath, Using = "//*/a[@class='tooltip-container']")]
        [CacheLookup]
        private IList<IWebElement> LabelTooltip;  // 2 items

        [FindsBy(How = How.XPath, Using = "//*/div[@class='tooltip-container']")]
        [CacheLookup]
        private IList<IWebElement> IconTooltip;  // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='tooltip tooltip--right tooltip--top']")]
        [CacheLookup]
        private IList<IWebElement> Tooltip;  // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='btn registration-form__block-continue-btn']")]
        [CacheLookup]
        private IList<IWebElement> ButtonContinue;  // 2 items 

        private string[] TextTitleBlockHeader = {
            "Za korisnike m:tel mobilne telefonije (rezidencijalni korisnici)",
            "Za korisnike m:tel fiksnih usluga (paketi, televizija, internet, telefonija)"
        };
        private string[] TextInfoBlockDescription =
        {
            "Preko m:tel mobilnog broja",
            "Preko šifre pretplatnika i poziva na broj koji možete pronaći na Vašem m:tel računu"
        };
        private string TextLabelPhone = "Broj telefona";
        private string TextLabelSmsCode = "SMS kod";
        private string TextLabelSubscription = "Šifra pretplatnika";
        private string TextLabelNumber = "Poziv na broj";
        private string TextLinkToLogin = "Nazad na prijavu";
        private string TextButtonContinue = "Nastavi";
        private string[] TextLabelTooltip = { "šifre pretplatnika", "poziva na broj" };
        private string[] TextIconTooltip = {
            "U ovo polje se upisuje šifra pretplatnika koja se nalazi na vašem računu.",
            "U ovo polje se upisuje poziv na broj koji se nalazi na vašem računu."
        };

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "MtbCustomerNotFoundError":
                    return PageMessages.Registration_MtbCustomerNotFoundError;
                case "notB2Ccustomer":
                    return PageMessages.Registration_notB2Ccustomer;
                case "OldAccount":
                    return PageMessages.Registration_OldAccount;
                case "SmsSent":
                    return PageMessages.Registration_SmsSent;
                case "OtpWrongFormat":
                    return PageMessages.Registration_OtpWrongFormat;
                case "LimitOtpCode":
                    return PageMessages.Registration_LimitOtpCode;
                case "OldOtpCode":
                    return PageMessages.Registration_OldOtpCode;
                case "notB2Cphone":
                    return PageMessages.Registration_notB2Cphone;
                case "PhoneNotFound":
                    return PageMessages.Registration_PhoneNotFound;
                case "InvalidPassword":
                    return PageMessages.InvalidPassword;
            };
            return "";
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitleBlockHeader[0], TitleBlockHeader[0].Text, "Wrong text for Mobile header"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitleBlockHeader[1], TitleBlockHeader[1].Text, "Wrong text for Fix header"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInfoBlockDescription[0], InfoBlockDescription[0].Text, "Wrong text for Mobile description"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInfoBlockDescription[1], InfoBlockDescription[1].Text, "Wrong text for Fix description"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPhone, LabelPhone.Text, "Wrong text for Name of Phone field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelSmsCode, LabelSmsCode.Text, "Wrong text for Name of Sms Code field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelSubscription, LabelSubscription.Text, "Wrong text for Name of Subscription field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelNumber, LabelNumber.Text, "Wrong text for Name of Number field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkToLogin, LinkToLogin.Text, "Wrong Link text"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonContinue[0].Text, "Wrong the 1st button text"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonContinue[1].Text, "Wrong the 2d button text"));
        }

        public void CheckTooltips(IWebDriver driver, AssertsHelper _assertsAccumulator)
        {
            Helper.HoverCursorOnWeb(driver, IconTooltip[0]);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextIconTooltip[0], Tooltip[0].Text, "Wrong the 1st icon tooltip"));
            Helper.HoverCursorOnWeb(driver, IconTooltip[1]);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextIconTooltip[1], Tooltip[1].Text, "Wrong the 2d icon tooltip"));

            Helper.HoverCursorOnWeb(driver, LabelTooltip[0]);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelTooltip[0], LabelTooltip[0].Text, "Wrong text for the 1st tooltip"));
            Helper.HoverCursorOnWeb(driver, LabelTooltip[1]);
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelTooltip[1], LabelTooltip[1].Text, "Wrong text for the 2d tooltip"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPhone, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPhone)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isWebDisabled(InputSmsCode),
                string.Format("Field {0} failed: is enabled", TextLabelSmsCode)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputSubscription, "12as*#", MsgError[2]),
                string.Format("Field {0} failed: is not manadatory", TextLabelSubscription)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputNumber, "12as*#", MsgError[3]),
                string.Format("Field {0} failed: is not manadatory", TextLabelNumber)));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isMobileServiceButtonDisabled(), "Button Continue Mobile failed: is enabled"));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isFixedServiceButtonDisabled(), "Button Continue Fixed failed: is enabled"));
        }

        public void EnterSubscription(string inputValue)
        {
            TestsHelper.EnterValue(InputSubscription, inputValue);
        }

        public void EnterNumber(string inputValue)
        {
            TestsHelper.EnterValue(InputNumber, inputValue);
        }

        public void EnterPhone(string inputValue)
        {
            TestsHelper.EnterValue(InputPhone, inputValue);
        }

        public void EnterOtp(string inputValue)
        {
            TestsHelper.EnterValue(InputSmsCode, inputValue);
        }

        public bool isLinkSmsEnabled()
        {
            return TestsHelper.isWebEnabled(LinkSmsSend);
        }

        public bool isLinkSmsDisabled()
        {
            return TestsHelper.isWebDisabled(LinkSmsSend);
        }

        public string GetMsgInputPhone()
        {
            return MsgError[0].Text;
        }

        public bool isMobileServiceButtonEnabled()
        {
            return TestsHelper.isWebEnabled(ButtonContinue[0]);
        }

        public bool isFixedServiceButtonEnabled()
        {
            return TestsHelper.isWebEnabled(ButtonContinue[1]);
        }

        public bool isMobileServiceButtonDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[0]);
        }

        public bool isFixedServiceButtonDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[1]);
        }

        public void ClickMobileServiceButton()
        {
            TestsHelper.ClickWebAfterWebNotExist(ButtonContinue[0], Notification);
        }

        public void ClickFixedServiceButton()
        {
            TestsHelper.ClickWebAfterWebNotExist(ButtonContinue[1], Notification);
        }

        public void ClickLinkToLogin()
        {
            TestsHelper.ClickWeb(LinkToLogin);
        }

        public void ClickLinkSmsSend()
        {
            TestsHelper.ClickWeb(LinkSmsSend);
        }
    }
}
