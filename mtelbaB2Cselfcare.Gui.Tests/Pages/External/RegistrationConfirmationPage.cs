﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class RegistrationConfirmationPage : PagesConfirmation
    {
        public RegistrationConfirmationPage(IWebDriver driver) : base(driver)
        { }

        private string TextTitle = "E-mail potvrda";
        private string[] TextInfo = {
            "Poslali smo Vam e-mail na ", //petar.peric@gmail.com
            " u kojem možete pronaći LINK za aktivaciju naloga. Ukoliko ga ne možete pronaći u inbox-u, provjerite spam folder ili pošaljite zahtjev ponovo."
        };
        private string TextButton = "Pošalji ponovo";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            CheckLabels(_assertsAccumulator, TextTitle, TextInfo, TextButton);
        }

        public string GetNotificationMsg(string typeMsg, string textMsg)
        {
            switch (typeMsg)
            {
                case "NoToken":
                    return PageMessages.RegistrationConfirmation_NoToken;
                case "EmaiSent":
                    return string.Format(PageMessages.RegistrationConfirmation_EmailSent, textMsg);
            };
            return "";
        }
    }
}
