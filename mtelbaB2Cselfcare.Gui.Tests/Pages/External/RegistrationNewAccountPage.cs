﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class RegistrationNewAccountPage : PagesExternal
    {
        public RegistrationNewAccountPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-create-account']")]
        [CacheLookup]
        private IWebElement NewAccountForm;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-title']")]
        [CacheLookup]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-subtitle registration-create-account__subtitle']")]
        [CacheLookup]
        private IWebElement SubTitle;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'firstname']")]
        [CacheLookup]
        private IWebElement LabelFirstName;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'firstname']")]
        [CacheLookup]
        private IWebElement InputFirstName;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'lastname']")]
        [CacheLookup]
        private IWebElement LabelLastName;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'lastname']")]
        [CacheLookup]
        private IWebElement InputLastName;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'password_repeat']")]
        [CacheLookup]
        private IWebElement LabelPasswordRepeat;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'password_repeat']")]
        [CacheLookup]
        private IWebElement InputPasswordRepeat;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'icon-checkbox icon')]")]
        [CacheLookup]
        private IWebElement Checkbox;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-checkbox icon-unchecked-checkbox']")]
        [CacheLookup]
        private IWebElement CheckboxFalse;

        [FindsBy(How = How.XPath, Using = "//*[@class='icon-checkbox icon-checked-checkbox']")]
        [CacheLookup]
        private IWebElement CheckboxTrue;

        [FindsBy(How = How.XPath, Using = "//*[@class='label-name']")]
        [CacheLookup]
        private IWebElement LabelCheckbox;

        [FindsBy(How = How.XPath, Using = "//*[@class='back-btn registration-layout__back-btn']")]
        [CacheLookup]
        private IWebElement Link;

        private string TextTitle = "Kreiranje naloga";
        private string TextSubTitle = "Unesite vaše podatke kako bismo mogli da Vam kreiramo nalog";
        private string TextLabelFirstName = "Ime";
        private string TextLabelLastName = "Prezime";
        private string TextLabelPasswordRepeat = "Potvrdite lozinku";
        private string TextLabelCheckbox = "Slažem se sa uslovima korišćenja bez ograničenja.";
        private string TextLink = "Nazad na registraciju";
        private string TextButton = "Kreiraj nalog";

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "InvalidPassword":
                    return PageMessages.InvalidPassword;
            };
            return "";
        }

        public bool isFormPageLoaded()
        {
            return TestsHelper.isWebExist(NewAccountForm);
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputFirstName, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelFirstName)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputLastName, "12", MsgError[1]),
                string.Format("Field {0} failed: is not manadatory", TextLabelLastName)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputEmail, "12", MsgError[2]),
                string.Format("Field {0} failed: is not manadatory", TextLabelEmail)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPassword, "12", MsgError[3]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPassword)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPasswordRepeat, "12", MsgError[3]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPasswordRepeat)));

            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonDisabled(), "Button Create failed: is enabled"));
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextSubTitle, SubTitle.Text, "Wrong text for subtitle"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelFirstName, LabelFirstName.Text, "Wrong label for first name field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelLastName, LabelLastName.Text, "Wrong label for last name field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelEmail, LabelEmail.Text, "Wrong label for Email field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPassword, LabelPassword.Text, "Wrong label for password field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPasswordRepeat, LabelPasswordRepeat.Text, "Wrong label for password repeat field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelCheckbox, LabelCheckbox.Text, "Wrong text for the checkbox"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLink, Link.Text, "Wrong text for link to Registration page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButton, Button.Text, "Wrong text for the button"));
        }

        public void EnterFirstName(string inputValue)
        {
            Helper.EnterValue(InputFirstName, inputValue);
        }

        public void EnterLastName(string inputValue)
        {
            Helper.EnterValue(InputLastName, inputValue);
        }

        public void EnterPasswordRepeat(string inputValue)
        {
            Helper.EnterValue(InputPasswordRepeat, inputValue);
        }

        public void ClickCheckbox()
        {
            Helper.ClickWeb(Checkbox);
        }

        public string GetMsgInputEmail()
        {
            return MsgError[2].Text;
        }
    }
}
