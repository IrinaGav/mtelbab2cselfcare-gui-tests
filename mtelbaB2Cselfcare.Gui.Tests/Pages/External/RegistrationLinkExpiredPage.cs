﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class RegistrationLinkExpiredPage : PagesConfirmation
    {
        public RegistrationLinkExpiredPage(IWebDriver driver): base(driver)
        { }

        private string TextTitle = "Vaš link je istekao.";
        private string TextInfo = "Trajanje linka je ograničeno na 24h. Ukoliko u tom vremenu ne potvrdite nalog, neophodno je ponovo pokrenuti proces registracije.";

        private string TextButton = "Pokrenite ponovo";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            CheckLabels(_assertsAccumulator, TextTitle, TextInfo, TextButton);
        }
    }
}
