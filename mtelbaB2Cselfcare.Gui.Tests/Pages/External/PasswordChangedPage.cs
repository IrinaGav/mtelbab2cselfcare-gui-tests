﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using OpenQA.Selenium;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PasswordChangedPage : PagesConfirmation
    {
        public PasswordChangedPage(IWebDriver driver) : base(driver)
        { }

        private string TextTitle = "Uspješno ste promijenili lozinku!";
        private string TextInfo = "Nastavite sa prijavom na Moj m:tel portal.";
        private string TextButton = "Prijavite se";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            CheckLabels(_assertsAccumulator, TextTitle, TextInfo, TextButton);
        }
    }
}
