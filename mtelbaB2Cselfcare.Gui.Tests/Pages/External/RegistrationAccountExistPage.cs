﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class RegistrationExistAccountPage : PagesExternal
    {
        public RegistrationExistAccountPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-account-exists registration-layout']")]
        [CacheLookup]
        private IWebElement AccountExistForm;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-title']")]
        [CacheLookup]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-layout__header-subtitle registration-account-exists__header-subtitle']")]
        [CacheLookup]
        private IWebElement SubTitle;

        [FindsBy(How = How.XPath, Using = "//*/div[contains(@class,'registration-account-exists__desktop-container')]//*/div[@class='radio-card__container-text']")]
        private IList<IWebElement> RadioOptions;  // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='registration-account-exists__list-text']")]    //    "")]
        private IList<IWebElement> ListItemAccountExistForm;  // 1 items / 3 items   */

        [FindsBy(How = How.XPath, Using = "//*/div[contains(@class,'registration-account-exists__desktop-container')]//*[contains(@class,'icon-checkmark')]")]
        private IList<IWebElement> OptionsToSelect;  // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='link']")]
        [CacheLookup]
        private IWebElement Link;

        [FindsBy(How = How.XPath, Using = "//*/button[@class='btn registration-layout__submit registration-account-exists__submit']")]
        private IWebElement ButtonSubmit;

        private string TextTitle = "Pronađena usluga";
        private string TextSubTitle = "Usluga je već registrovana na moj m:tel nalogu: {0}. Odaberite opciju:";
        private string[] TextRadioAccountExistForm = { "Uloguj me sa\r\n{0}", "Registruj ovu uslugu na novi nalog" };
        private string[] TextListItemToLogin = { "Unosom lozinke pristupićete Moj m:tel nalogu." };
        private string[] TextListItemCreationNotPossible = {
            "Nije moguće ukloniti uslugu sa datog naloga.",
            "Izaberite prvu opciju i ulogujte se sa postojećim nalogom."
        };
        private string[] TextListItemToCreate = {
            "Ovaj nalog će vam biti razvezan sa {0} .",
            "Unesite lozinku postojećeg naloga iz kojeg želite ukloniti uslugu.",
            "Kreirajte novi nalog."
        };

        private string TextButtonLogin = "Uloguj se";
        private string TextButtonContinue = "Nastavi";

        public bool isFormPageLoaded()
        {
            return TestsHelper.isWebExist(AccountExistForm);
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonSubmitDisabled(), "Button Login failed: is enabled"));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPassword, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPassword)));
        }

        public void CheckLabelsExistMain(AssertsHelper _assertsAccumulator, string user)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextSubTitle, user), SubTitle.Text, "Wrong text for subtitle"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPassword, LabelPassword.Text, "Wrong link text"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkForgotPassword, Link.Text, "Wrong link text"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextRadioAccountExistForm[0], user), RadioOptions[0].Text, "Wrong text for the 1st radio button"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextRadioAccountExistForm[1], RadioOptions[1].Text, "Wrong text for the 2d radio button"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonLogin, ButtonSubmit.Text, "Wrong Login button text"));
            for (int i = 0; i < ListItemAccountExistForm.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextListItemToLogin[i], ListItemAccountExistForm[i].Text, "Wrong info text if Login is selected"));
            }
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPassword, LabelPassword.Text, "Wrong link text"));

            SelectNewAccount();

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonSubmit.Text, "Wrong Continue button text"));
            for (int i = 0; i < ListItemAccountExistForm.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextListItemCreationNotPossible[i], ListItemAccountExistForm[i].Text, "Wrong info text if New account is selected"));
            }
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonSubmitDisabled(), "Button Create new account failed: is enabled"));

            SelectLogin();
        }

        public void CheckLabelsExistNotMain(AssertsHelper _assertsAccumulator, string user)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextSubTitle, user), SubTitle.Text, "Wrong text for subtitle"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPassword, LabelPassword.Text, "Wrong link text"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLinkForgotPassword, Link.Text, "Wrong link text"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextRadioAccountExistForm[0], user), RadioOptions[0].Text, "Wrong text for the 1st radio button"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextRadioAccountExistForm[1], RadioOptions[1].Text, "Wrong text for the 2d radio button"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonLogin, ButtonSubmit.Text, "Wrong Login button text"));
            for (int i = 0; i < ListItemAccountExistForm.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextListItemToLogin[i], ListItemAccountExistForm[i].Text, "Wrong info text if Login is selected"));
            }

            SelectNewAccount();
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonSubmit.Text, "Wrong Continue button text"));
            for (int i = 0; i < ListItemAccountExistForm.Count; i++)
            {
                string expectedText = (i == 0) ? string.Format(TextListItemToCreate[i], user) : TextListItemToCreate[i];
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(expectedText, ListItemAccountExistForm[i].Text, "Wrong info text if New account is selected"));
            }
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonSubmitDisabled(), "Button Create new account failed: is enabled"));

            SelectLogin();
        }

        public void SelectLogin()
        {
            TestsHelper.ClickWeb(OptionsToSelect[0]);
        }

        public void SelectNewAccount()
        {
            TestsHelper.ClickWeb(OptionsToSelect[1]);
        }

        public bool isButtonSubmitDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonSubmit);
        }

        public void ClickLinkToForgotPassword()
        {
            TestsHelper.ClickWeb(Link);
        }

        public void ClickButtonLogin()
        {
            TestsHelper.ClickWeb(ButtonSubmit);
        }
    }
}
