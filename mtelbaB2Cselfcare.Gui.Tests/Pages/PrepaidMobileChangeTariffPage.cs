﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PrepaidMobileChangeTariffPage : PagesInner
    {
        public PrepaidMobileChangeTariffPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='card__content']")]
        private IList<IWebElement> Tariffs;  // 2

        [FindsBy(How = How.XPath, Using = "//*[@class='card__title']")]
        private IList<IWebElement> TariffName;  // 2

        [FindsBy(How = How.XPath, Using = "//*[@class='card__description']")]
        private IList<IWebElement> TariffDescription;  // 2

        [FindsBy(How = How.XPath, Using = "//*[@class='btn card__btn']")]
        private IList<IWebElement> TariffButton;  // 2

        [FindsBy(How = How.XPath, Using = "//*[@class='sub-nav-overview__service-item active']//preceding-sibling::div[@class='sub-nav-overview__account-tariff']")]
        private IWebElement ActiveTariff;

        private string TextTariffButton = "Podnesite zahtjev";
        private string TextPopupTitle = "";

        public void ClickTariffButton(int index)
        {
            TestsHelper.ClickWeb(TariffButton[index]);
        }

        public int GetTariffsCount()
        {
            return Tariffs.Count;
        }

        public string GetTariffName(int index)
        {
            return TariffName[index].Text;
        }

        public string GetActiveTariffName()
        {
            return ActiveTariff.Text;
        }

        public void CheckElements(AssertsHelper _assertsAccumulator)
        {
            foreach (var item in TariffName)
            {
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(item.Text.Length > 0, "One of tariffs doesn't have name"));
            }
            foreach (var item in TariffDescription)
            {
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(item.Text.Length > 0, "One of tariffs doesn't have description"));
            }
            foreach (var item in TariffButton)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTariffButton, item.Text, "One of tariff button has incorrect label"));
            }
        }
    }
}
