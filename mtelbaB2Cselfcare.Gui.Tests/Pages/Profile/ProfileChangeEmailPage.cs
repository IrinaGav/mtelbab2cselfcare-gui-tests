﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ProfileChangeEmailPage : PagesInner
    {
        public ProfileChangeEmailPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*/button[@class='page-change-email__submit btn']")]
        private IWebElement ButtonChangeEmail;

        private string TextTitle = "PROMJENA E-MAIL ADRESE";
        private string TextButtonChangeEmail = "Izmjenite e-mail";

        private string TextLabelNewEmail = "Nova e-mail adresa";
        private string TextPopupTitle = "E-mail potvrda";
        private string TextPopupInfo = "Poslali smo e-mail na {0} sa linkom za promjenu e-mail adrese. Ako ga ne možete pronaći u inbox-u, pogledajte SPAM folder ili ponovo pošaljite Zahtjev.";
        private string TextPopupButton = "Pokušajte ponovo";

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "InvalidPassword":
                    return PageMessages.InvalidPassword;
                case "EmailSent":
                    return PageMessages.EmailChangeConfirmation_EmailSent;
            };
            return "";
        }

        public string GetMsgInputEmail()
        {
            return MsgError[0].Text;
        }

        public string GetMsgInputPassword()
        {
            return MsgError[1].Text;
        }

        public void ClickButtonChangeEmail()
        {
            TestsHelper.ClickWeb(ButtonChangeEmail);
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelYourPassword, LabelPassword.Text, "Wrong text for input Password"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelNewEmail, LabelEmail.Text, "Wrong text for input Email"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonChangeEmail, ButtonChangeEmail.Text, "Wrong text for button"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputEmail, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelNewEmail)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPassword, "12", MsgError[1]),
                string.Format("Field {0} failed: is not manadatory", TextLabelYourPassword)));
        }

        public void CheckLabelsPopup(AssertsHelper _assertsAccumulator, string user)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupTitle, PopupTitle.Text, "Confirmation popup: wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextPopupInfo, user), PopupInfo.Text, "Confirmation popup: wrong text for info"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupButton, PopupButton.Text, "Confirmation popup: wrong text for button"));
        }
    }
}
