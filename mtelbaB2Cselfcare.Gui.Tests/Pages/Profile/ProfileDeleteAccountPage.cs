﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ProfileDeleteAccountPage : PagesInner
    {
        public ProfileDeleteAccountPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-title page-delete-account__info-component-text-title']")]
        protected IWebElement PageTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-description page-delete-account__info-component-text-description']")]
        protected IWebElement PageInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn']")]
        protected IWebElement PageButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[@class='btn']")]
        protected IWebElement PopupButton;

        private string TextPageTitle = "Brisanje Moj m:tel naloga";
        private string TextPageInfo = "Brisanjem prestajete koristiti Moj m:tel nalog i morate se ponovo registrovati.";
        private string TextButton = "Obrišite nalog";

        private string TextPopupTitle = "Da li ste sigurni da želite da obrišete Moj m:tel nalog?";
        private string TextPopupInfo = "Brisanjem prestajete koristiti Moj m:tel nalog i morate se ponovo registrovati.";

        public void ClickPageButton()
        {
            TestsHelper.ClickWeb(PageButton);
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageTitle, PageTitle.Text, "Wrong text for title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageInfo, PageInfo.Text, "Wrong text for info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButton, PageButton.Text, "Wrong text for button on page"));
        }

        public void CheckLabelsPopup(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupTitle, PopupTitle.Text, "Wrong text for title in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupInfo, PopupInfo.Text, "Wrong text for info in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButton, PopupButton.Text, "Wrong text for button in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, PopupButtonCancel.Text, "Wrong text for button Cancel in popup"));
        }
    }
}
