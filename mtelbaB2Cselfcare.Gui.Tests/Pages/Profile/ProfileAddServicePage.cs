﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ProfileAddServicePage : PagesInner
    {
        public ProfileAddServicePage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__title']")]
        [CacheLookup]
        protected IWebElement AddServiceTitle;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'add-service__tabs-control-btn')]")]
        [CacheLookup]
        protected IList<IWebElement> AddServiceTabs;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'phone']")]
        [CacheLookup]
        private IWebElement LabelPhone;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'phone']")]
        [CacheLookup]
        private IWebElement InputPhone;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__send-sms-btn']")]
        [CacheLookup]
        private IWebElement LinkSmsSend;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'sms_code']")]
        [CacheLookup]
        private IWebElement LabelSmsCode;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'sms_code']")]
        [CacheLookup]
        private IWebElement InputSmsCode;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'subscriber_code']")]
        [CacheLookup]
        private IWebElement LabelSubscription;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'subscriber_code']")]
        [CacheLookup]
        private IWebElement InputSubscription;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'reference_number']")]
        [CacheLookup]
        private IWebElement LabelNumber;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'reference_number']")]
        [CacheLookup]
        private IWebElement InputNumber;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn add-service__btn-submit']")]
        [CacheLookup]
        private IList<IWebElement> ButtonContinue;   // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='btn btn--transparent add-service__btn-cancel']")]
        [CacheLookup]
        private IList<IWebElement> ButtonCancel;   // 2 items

        [FindsBy(How = How.XPath, Using = "//*[@class='btn add-service__connect-btn-submit']")]
        [CacheLookup]
        private IWebElement ButtonConnect;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__exist']/div[@class='add-service__title']")]
        private IWebElement AddServiceExistTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__exist']/div[@class='add-service__subtitle']")]
        private IWebElement AddServiceExistSubtitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__exist']/p")]
        private IList<IWebElement> AddServiceExistInfo;   // 2 items

        [FindsBy(How = How.XPath, Using = "//*/button[contains(@class,'add-service__btn-cancel')]")]
        private IWebElement AddServiceExistButtonCancel;

        [FindsBy(How = How.XPath, Using = "//*/button[contains(@class,'add-service__btn-submit')]")]
        private IWebElement AddServiceExistButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='add-service__connect-desc']")]
        private IWebElement AddingServiceInfo;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'firstName']")]
        private IWebElement InputName;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'lastName']")]
        private IWebElement InputLastName;

        private string TextTitle = "Izaberite uslugu koju želite dodati";
        private string[] TextTabs = { "MOBILNA USLUGA", "FIKSNA USLUGA" };
        private string TextLabelPhone = "Broj telefona";
        private string TextLabelSmsCode = "SMS kod";
        private string TextLabelSubscription = "Šifra pretplatnika";
        private string TextLabelNumber = "Poziv na broj";
        private string TextButtonContinue = "Nastavite";

        private string TextAddServiceExistTitle = "Pronađena usluga";
        private string TextAddServiceExistSubtitle = "MOBILNA USLUGA";
        private string[] TextAddServiceExistInfo= {
            "Usluga je već uvezana na nalogu {0}. Ukoliko želite da premjestite uslugu na trenutni nalog, potrebno je da unesete lozinku gore pomenutog naloga.",
            "Unosom lozinke usluga će biti uklonjena sa naloga {0} i biće dodata na Vaš trenutni nalog." };
        private string TextAddServiceExistButtonCancel = "Korak nazad";

        private string TextAddingServiceInfo = "Uspešno ste uvezali uslugu na vaš nalog. Ovde možete promeniti vaše ime i prezime.";

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "MtbCustomerNotFoundError":
                    return PageMessages.Registration_PhoneNotFound;
                case "notB2Ccustomer":
                    return PageMessages.AddFixedService_NotB2C;
                case "OldAccount":
                    return PageMessages.Registration_OldAccount;
                case "SmsSent":
                    return PageMessages.Registration_SmsSent;
                case "OtpWrongFormat":
                    return PageMessages.Registration_OtpWrongFormat;
                case "LimitOtpCode":
                    return PageMessages.Registration_LimitOtpCode;
                case "OldOtpCode":
                    return PageMessages.Registration_OldOtpCode;
                case "notB2Cphone":
                    return PageMessages.AddFixedService_NotB2C;
                case "PhoneNotFound":
                    return PageMessages.Registration_PhoneNotFound;
                case "ServiceIsConnected":
                    return PageMessages.AddService_AlreadyIsConnected;
                case "InvalidPassword":
                    return PageMessages.InvalidPassword;
                case "MainService":
                    return PageMessages.AddService_IsMain;
                case "Unlinked":
                    return PageMessages.AddedService_Unlinked;
                case "Linked":
                    return PageMessages.AddedService_Linked;
            };
            return "";
        }

        public bool isAddServiceOpened()
        {
            return TestsHelper.GetWebText(AddServiceTitle).Length > 0;
        }

        public void activateAddMobileServiceTab()
        {
            TestsHelper.ClickWeb(AddServiceTabs[0]);
        }

        public void activateAddFixedServiceTab()
        {
            TestsHelper.ClickWeb(AddServiceTabs[1]);
        }

        public bool isButtonContinueMobileDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[0]);
        }

        public bool isButtonContinueFixedDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[1]);
        }

        public void CheckLabelsAddService(AssertsHelper _assertsAccumulator)
        {
            activateAddMobileServiceTab();
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, AddServiceTitle.Text, "Wrong text for Add Service title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTabs[0], AddServiceTabs[0].Text, "Wrong text for Mobile tab"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTabs[1], AddServiceTabs[1].Text, "Wrong text for Fixed tab"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPhone, LabelPhone.Text, "Wrong text for Name of Phone field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelSmsCode, LabelSmsCode.Text, "Wrong text for Name of Sms Code field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonContinue[0].Text, "Wrong the button text for Mobile service"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, ButtonCancel[0].Text, "Wrong the cancel button text for Mobile service"));

            activateAddFixedServiceTab();
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelSubscription, LabelSubscription.Text, "Wrong text for Name of Subscription field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelNumber, LabelNumber.Text, "Wrong text for Name of Number field"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, ButtonContinue[1].Text, "Wrong the button text for Fixed service"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, ButtonCancel[1].Text, "Wrong the cancel button text for Fixed service"));
        }

        public void CheckMandatoryInputsAddService(AssertsHelper _assertsAccumulator)
        {
            activateAddMobileServiceTab();
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputPhone, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelPhone)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isWebDisabled(InputSmsCode),
                string.Format("Field {0} failed: is enabled", TextLabelSmsCode)));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonContinueMobileDisabled(), "Button Continue Mobile failed: is enabled"));

            activateAddFixedServiceTab();
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputSubscription, "12as*#", MsgError[2]),
                string.Format("Field {0} failed: is not manadatory", TextLabelSubscription)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputNumber, "12as*#", MsgError[3]),
                string.Format("Field {0} failed: is not manadatory", TextLabelNumber)));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(isButtonContinueFixedDisabled(), "Button Continue Fixed failed: is enabled"));
        }

        public void CheckLabelsAddServiceExist(AssertsHelper _assertsAccumulator, string user)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAddServiceExistTitle, AddServiceExistTitle.Text, "Wrong text for Add Service Exist title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAddServiceExistSubtitle,AddServiceExistSubtitle.Text, "Wrong text for Add Service Exist subtitle"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextAddServiceExistInfo[0], user), AddServiceExistInfo[0].Text, "Wrong text for 1st part of info"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextAddServiceExistInfo[1], user), AddServiceExistInfo[1].Text, "Wrong text for 2d part of info"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelPassword, LabelPassword.Text, "Wrong text for password field"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAddServiceExistButtonCancel, AddServiceExistButtonCancel.Text, "Wrong text for Cancel button"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonContinue, AddServiceExistButton.Text, "Wrong text for Continue button"));
        }

        public void EnterSubscription(string inputValue)
        {
            TestsHelper.EnterValue(InputSubscription, inputValue);
        }

        public void EnterNumber(string inputValue)
        {
            TestsHelper.EnterValue(InputNumber, inputValue);
        }

        public void EnterPhone(string inputValue)
        {
            TestsHelper.EnterValue(InputPhone, inputValue);
        }

        public void EnterOtp(string inputValue)
        {
            TestsHelper.EnterValue(InputSmsCode, inputValue);
        }

        public bool isLinkSmsEnabled()
        {
            return TestsHelper.isWebEnabled(LinkSmsSend);
        }

        public bool isLinkSmsDisabled()
        {
            return TestsHelper.isWebDisabled(LinkSmsSend);
        }

        public bool isMobileServiceButtonEnabled()
        {
            return TestsHelper.isWebEnabled(ButtonContinue[0]);
        }

        public bool isFixedServiceButtonEnabled()
        {
            return TestsHelper.isWebEnabled(ButtonContinue[1]);
        }

        public bool isMobileServiceButtonDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[0]);
        }

        public bool isFixedServiceButtonDisabled()
        {
            return TestsHelper.isWebDisabled(ButtonContinue[1]);
        }

        public void ClickMobileServiceButton()
        {
            TestsHelper.ClickWebAfterWebNotExist(ButtonContinue[0], Notification);
        }

        public void ClickFixedServiceButton()
        {
            TestsHelper.ClickWeb(ButtonContinue[1]);
        }

        public void ClickLinkSmsSend()
        {
            TestsHelper.ClickWeb(LinkSmsSend);
        }

        public void ClickMobileCancelButton()
        {
            TestsHelper.ClickWeb(ButtonCancel[0]);
        }

        public void ClickAddServiceExistButton()
        {
            TestsHelper.ClickWebAfterWebNotExist(AddServiceExistButton, Notification);
        }

        public void ClickAddServiceConnectButton()
        {
            TestsHelper.ClickWebAfterWebNotExist(ButtonConnect, Notification);
        }

        public void EnterName(string inputValue)
        {
            TestsHelper.EnterValue(InputName, inputValue);
        }

        public void EnterLastName(string inputValue)
        {
            TestsHelper.EnterValue(InputLastName, inputValue);
        }

        public string GetMsgInputPhone()
        {
            return MsgError[0].Text;
        }
    }
}
