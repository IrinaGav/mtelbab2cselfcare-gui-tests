﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ProfileChangePasswordPage : PagesInner
    {
        public ProfileChangePasswordPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='page-change-password__description']")]
        [CacheLookup]
        private IWebElement Description;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'old_password']")]
        [CacheLookup]
        private IWebElement LabelOldPassword;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'old_password']")]
        private IWebElement InputOldPassword;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'new_password']")]
        [CacheLookup]
        private IWebElement LabelNewPassword;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'new_password']")]
        private IWebElement InputNewPassword;

        [FindsBy(How = How.XPath, Using = labelPrefix + "'confirm_password']")]
        [CacheLookup]
        private IWebElement LabelConfirmPassword;

        [FindsBy(How = How.XPath, Using = inputPrefix + "'confirm_password']")]
        private IWebElement InputConfirmPassword;

        [FindsBy(How = How.XPath, Using = "//*/button[@class='page-change-password__submit btn']")]
        private IWebElement ButtonChangePassword;

        private string TextTitle = "PROMJENA LOZINKE";
        private string TextDescription = "Lozinka treba da ima najmanje 8 karaktera i ispuni najmanje 2 uslova: veliko slovo, malo slovo, broj, simbol";
        private string TextLabelOldPassword = "Vaša lozinka";
        private string TextLabelNewPassword = "Nova lozinka";
        private string TextLabelConfirmPassword = "Potvrdite lozinku";
        private string TextButtonChangePassword = "Sačuvajte lozinku";

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "InvalidOldPassword":
                    return PageMessages.InvalidOldPassword;
                case "UserNameInPassword":
                    return PageMessages.UserNameInPassword;
                case "PasswordIsOld":
                    return PageMessages.PasswordIsOld;
                case "PasswordIsChanged":
                    return PageMessages.PasswordIsChanged;
            };
            return "";
        }

        public string GetMsgInputOldPassword()
        {
            return MsgError[0].Text;
        }

        public string GetMsgInputNewPassword()
        {
            return MsgError[1].Text;
        }

        public string GetMsgInputConfirmPassword()
        {
            return MsgError[2].Text;
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle, Title.Text, "Wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextDescription, Description.Text, "Wrong text for description"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelOldPassword, LabelOldPassword.Text, "Wrong text for input Old password"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelNewPassword, LabelNewPassword.Text, "Wrong text for input New password"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextLabelConfirmPassword, LabelConfirmPassword.Text, "Wrong text for input Confirm password"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonChangePassword, ButtonChangePassword.Text, "Wrong text for button"));
        }

        public void CheckMandatoryInputs(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputOldPassword, "12", MsgError[0]),
                string.Format("Field {0} failed: is not manadatory", TextLabelOldPassword)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputNewPassword, "12", MsgError[1]),
                string.Format("Field {0} failed: is not manadatory", TextLabelNewPassword)));
            _assertsAccumulator.Accumulate(() => Assert.That(TestsHelper.isMandatoryInput(InputConfirmPassword, "12", MsgError[2]),
                string.Format("Field {0} failed: is not manadatory", TextLabelConfirmPassword)));
        }

        public void EnterOldPassword(string inputValue)
        {
            TestsHelper.EnterValue(InputOldPassword, inputValue);
        }

        public void EnterNewPassword(string inputValue)
        {
            TestsHelper.EnterValue(InputNewPassword, inputValue);
        }

        public void EnterConfirmPassword(string inputValue)
        {
            TestsHelper.EnterValue(InputConfirmPassword, inputValue);
        }

        public void ClickButtonChangePassword()
        {
            TestsHelper.ClickWeb(ButtonChangePassword);
        }
    }
}
