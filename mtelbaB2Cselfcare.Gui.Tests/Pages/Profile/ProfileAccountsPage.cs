﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class ProfileAccountsPage : PagesInner
    {
        public ProfileAccountsPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='page-linked-accounts__block']/div[@class='main-layout__title']")]
        [CacheLookup]
        private IList<IWebElement> PageTitle;  // 2

        [FindsBy(How = How.XPath, Using = "//*[@class='page-linked-accounts__item-info']/div[@class='page-linked-accounts__item-name']")]
        private IList<IWebElement> ListNames;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-linked-accounts__item-info']/div[@class='page-linked-accounts__item-tariffs']")]
        private IList<IWebElement> ListTariffs;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-linked-accounts__button btn--white']")]
        private IList<IWebElement> ListUnlinkLinks;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[@class='btn']")]
        protected IWebElement PopupButton;

        private string[] TextTitle = { "OSNOVNI NALOG", "UVEZANI NALOZI" };
        private string TextUnlinkLink = "Uklonite uvezani nalog";

        private string TextPopupTitle = "Da li ste sigurni da želite da uklonite uvezani nalog?";
        private string TextPopupInfo = "Uklanjanjem uvezanog naloga pristup pregledu resursa i ostalim benefitima neće biti dostupan.";
        private string TextPopupButton = "Uklonite nalog";

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle[0], PageTitle[0].Text, "Wrong text for main title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTitle[1], PageTitle[1].Text, "Wrong text for the list title"));
            foreach (var item in ListUnlinkLinks)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextUnlinkLink, item.Text, "Wrong text for one of the links"));
            }
        }

        public void CheckLabelsPopup(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupTitle, PopupTitle.Text, "Confirmation popup: wrong text for title"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupInfo, PopupInfo.Text, "Confirmation popup: wrong text for info"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupButton, PopupButton.Text, "Confirmation popup: wrong text for button"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, PopupButtonCancel.Text, "Confirmation popup: wrong text for button Cancel"));
        }

        public void CheckLinksCount(AssertsHelper _assertsAccumulator)
        {
            int AccountsCount = ListNames.Count;
            int LinksCount = ListUnlinkLinks.Count;
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(AccountsCount-1, LinksCount, "Some of the list items don't have the link"));
        }

        public void ClickUnlink(int index)
        {
            TestsHelper.ClickWeb(ListUnlinkLinks[index]);
        }

        public List<string> GetListServices()
        {
            List<string> list = new List<string>(ListNames.Count);
            foreach (var item in ListNames)
            {
                list.Add(item.Text);
            }
            return list;
        }
    }
}
