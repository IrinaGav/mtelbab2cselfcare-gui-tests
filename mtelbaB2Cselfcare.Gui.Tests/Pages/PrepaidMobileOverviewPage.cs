﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PrepaidMobileOverviewPage : PagesInner
    {
        public PrepaidMobileOverviewPage(IWebDriver driver) : base(driver)
        { }

        public string ServiceStateResources = "Resources";
        public string ServiceStateTariffPlanToActivate = "AvailableTariffsToActivate";
        public string ServiceStateGrace = "Grace";

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__header']")]
        private IList<IWebElement> BonusesHeader;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__resource-info is-desktop']/div[@class='bonuses__resource-name']")]
        private IList<IWebElement> BonusName;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses']//div[@class='bonuses__resource-info is-desktop']/div[@class='bonuses__resource-expiry-date']")]
        private IList<IWebElement> BonusExpiryDate;

        [FindsBy(How = How.XPath, Using = "//*[@class='progress-bar__filled progress-bar__filled--red']")]
        private IList<IWebElement> BonusesProgressBarRed;

        [FindsBy(How = How.XPath, Using = "//*[@class='progress-bar__filled progress-bar__filled--blue']")]
        private IList<IWebElement> BonusesProgressBarBlue;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses bonuses--options']//div[@class='text-info bonuses__option-date']")]
        private IList<IWebElement> OptionExpiryDate;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__resource-values']")]
        private IList<IWebElement> BonusesResourceValue;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__resource-amount']")]
        private IList<IWebElement> BonusesAbsValue;

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__resource-name is-desktop']")]
        private IList<IWebElement> TariffPlanResourceName;

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__resource-balance']")]
        private IList<IWebElement> TariffPlanResourceValue;

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__resource-balance-unlimited']")]
        private IList<IWebElement> TariffPlanResourceUnlim;

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__tariff-name']")]
        private IWebElement TariffPlanName;

        [FindsBy(How = How.XPath, Using = "//*[@class='text-info']")]
        private IWebElement TariffPlanNote;

        [FindsBy(How = How.XPath, Using = "//*[@class='toggle tariff-plan__toggle toggle--active-red']")]
        private IWebElement TariffDeactivationSwitcher;

        private string TextPopupDeactivateTitle = "Da li ste sigurni da želite deaktivirati {0} tarifni plan?";
        private string TextPopupDeactivateInfo = "Deaktivacijom plana gubite resurse iz {0} tarifnog plana.";
        private string TextPopupDeactivateButton = "Deaktivirajte";

        //// to activate tariff plan page

        [FindsBy(How = How.XPath, Using = "//*[@class='active-tariff']")]
        private IWebElement ActiveTariff;

        [FindsBy(How = How.XPath, Using = "//*[@class='active-tariff__title']")]
        private IWebElement ActiveTariffTitle;

        [FindsBy(How = How.XPath, Using = "//*/button[contains(@class,'carousel__dot')]")]
        private IList<IWebElement> TariffCardCrousel;

        [FindsBy(How = How.XPath, Using = "//*[@class='card active-tariff__card']")]
        private IList<IWebElement> ActiveTariffCard;

        private const string ActiveTariffCardTitleXPath = "//*[@class='card active-tariff__card'][{0}]//div[@class='card__title active-tariff__card-title']";
        [FindsBy(How = How.XPath, Using = "//*[@class='card active-tariff__card']//div[@class='card__title active-tariff__card-title']")]
        private IList<IWebElement> ActiveTariffCardTitle;

        private const string ActiveTariffCardCharacteristicXPath = "//*[@class='card active-tariff__card'][{0}]//div[@class='active-tariff__characteristic']";

        private const string ActiveTariffCardPriceXPath = "//*[@class='card active-tariff__card'][{0}]//div[@class='active-tariff__tariff-plan-price']";
        [FindsBy(How = How.XPath, Using = "//*[@class='card active-tariff__card']//div[@class='active-tariff__formatted-price']")]
        private IList<IWebElement> ActiveTariffCardPrice;

        [FindsBy(How = How.XPath, Using = "//*[@class='card active-tariff__card']//button[@class='btn card__btn active-tariff__btn card-btn']")]
        private IList<IWebElement> ActiveTariffCardButton;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal') and not(contains(@class,'warning'))]//div[contains(@class,'info-component__text-title')]")]
        protected IWebElement PopupTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//div[contains(@class,'info-component__text-description')]")]
        protected IWebElement PopupInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[@class='btn']")]
        protected IWebElement PopupButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//button[contains(@class,'btn btn--white')]")]
        protected IWebElement PopupButtonCancel;

        private string TextBannerHeaderActiveTariff = "Uz pretplatu dobijam najviše";
        private int BannerButtonsCntActiveTariff = 1;
        private string TextAvailableTariffPlansTitle = "Isplaniraj komunikaciju uz Dopuna tarifne planove";

        private string TextPopupActivateTitle = "Da li ste sigurni da želite da aktivirate {0} tarifni plan?";
        private string TextPopupActivateInfo = "Aktivacija {0} tarifnog plana će Vam biti naplaćena po cjeni od {1} KM.";
        private string TextPopupActivateButton = "Aktivirajte";

        //// Banner

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__info']")]
        private IWebElement Banner;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__header']")]
        private IWebElement BannerHeader;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__content']")]
        private IWebElement BannerInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__header-text']")]
        private IWebElement BannerInfoText;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__button-link']")]
        private IList<IWebElement> BannerButtons;

        private string TextResourcesBannerHeader = "Paket: S";
        private string TextResourcesBannerInfo = "Proširite svoj paket sa TV sadržajima. Aktivirajte videoteke ili sjajne pakete TV kanala iz m:tel ponude.";
        private string[] TextResourcesBannerButtons = { "Saznaj više" };

        private string TextAvailableTariffPlansBannerHeader = "Uz pretplatu dobijam najviše";
        private string TextAvailableTariffPlansBannerInfo = "Veći bonusi, besplatni pozivi ka prijatelj broju, jeftini razgovori i neograničeni mobilni internet.";
        private string[] TextAvailableTariffPlansBannerButtons = { "Saznajte više" };

        private string TextGraceActiveBannerHeader = "Vaš račun ističe {0}";
        private string TextGraceActiveBannerInfo = "Iskoristite Online dopunu ili kupovinu dodataka kako biste produžili trajanje Vašeg računa.";
        private string TextGraceActiveBannerInfoText = "Kako biste izašli iz grace statusa iskoristite jednu od usluga.";
        private string[] TextGraceActiveBannerButtons = { "Online dopuna", "Kupovina dodataka", "Kupovina tarifnog plana" };

        //// grace status page

        [FindsBy(How = How.XPath, Using = "//*[@class='grace-period']")]
        private IWebElement GracePeriod;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-title grace-period__info-component-text-title']")]
        private IWebElement GraceTitle;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component__text-description grace-period__info-component-text-description']")]
        private IWebElement GraceInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component grace-period__info-component']//button[@class='btn']")]
        private IWebElement GraceButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='info-component--modal info-component grace-period__modal-info-component info-component info-component--modal']")]
        protected IWebElement PopupGraceActivate;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal-info')]/div[contains(@class,'info-component__text-title')]")]
        protected IWebElement PopupGraceTitle;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal-info')]//div[contains(@class,'info-component__text-description')]")]
        protected IWebElement PopupGraceInfo;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal-info')]//button[@class='btn']")]
        protected IWebElement PopupGraceButton;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'info-component--modal') and contains(@class,'warning')]")]
        protected IWebElement PopupGraceWarning;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'warning')]/div[contains(@class,'info-component__text-title')]")]
        protected IWebElement PopupGraceWarningTitle;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'warning')]//div[contains(@class,'info-component__text-description')]")]
        protected IWebElement PopupGraceWarningInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='modal']//i[contains(@class,'modal__icon-close')]")]
        protected IWebElement PopupGraceWarningCloseButton;

        private string TextGraceTitle = "Trajanje vašeg računa je isteklo!";
        private string TextGraceInfo = "Produžite period važenja Vašeg glavnog računa kupovinom tarifne opcije od 0.5KM i Vaš račun će biti produžen za 3 dana. Ukoliko u roku od najviše 120 dana, od dana isteka dopune, ne produžite trajanje Vašeg računa broj ulazi u proceduru deaktivacije.";
        private string TextGraceButton = "Aktivirajte";

        private string TextPopupGraceTitle = "Da li želite da produžite trajanje glavnog računa?";
        private string TextPopupGraceInfo = "Tarifna opcija će Vam biti naplaćena 0.5KM sa Vašeg računa.";

        private string TextPopupGraceWarningTitle = "Vaš zahtjev nije prihvaćen";
        private string TextPopupGraceWarningInfo = "Poštovani korisniče nemate dovoljno novca na računu za kupovinu tarifne opcije. Molimo Vas dopunite račun i pokušajte ponovo.";

        //// grace status page

        public string GetNotificationMsg(string typeMsg)
        {
            switch (typeMsg)
            {
                case "PlanActivated":
                    return PageMessages.TariffPlanActivated;
                case "PlanDeactivated":
                    return PageMessages.TariffPlanDeactivated;
            };
            return "";
        }

        public string GetServiceStatus()
        {
            if (Helper.isWebExist(ActiveTariff))
            {
                return ServiceStateTariffPlanToActivate;
            }
            if (Helper.isWebExist(GracePeriod))
            {
                return ServiceStateGrace;
            }
            return ServiceStateResources;
        }

        public bool ActivateTariffPlan(int index)
        {
            ClickTariffPlanCardButton(index);
            isPageLoaded();
            if (isPopupDisplayed())
            {
                ClickPopupButton();
                isPageLoaded();
                return true;
            }
            return false;
        }

        public bool DeactivateTariffPlan()
        {
            ClickDeactivationSwitcher();
            isPageLoaded();
            if (isPopupDisplayed())
            {
                ClickPopupButton();
                isPageLoaded();
                return true;
            }
            return false;
        }

        public bool ChangeServiceStatusTo(string status)
        {
            if (GetServiceStatus().Equals(status))
            {
                return true;
            }
            if (status.Equals(ServiceStateResources))
            {
                try
                {
                    ActivateTariffPlan(0);
                    return true;
                }
                catch (Exception)
                {
                    Assert.Fail("There is no one available tariff plan. Test is interrrupted");
                }
            }
            if (status.Equals(ServiceStateTariffPlanToActivate))
            {
                DeactivateTariffPlan();
                return true;
            }
            return false;
        }

        public void CheckResources(AssertsHelper _assertsAccumulator, string serviceId)
        {
            var testData = new PrepaidMobileRersoucesTestData(serviceId);

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesHeader.Length, BonusesHeader.Count, "Count of Bonuses Header failed"));
            for (int i = 0; i < BonusesHeader.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesHeader[i], BonusesHeader[i].Text,
                    string.Format("Bonuses header {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusName.Length, BonusName.Count, "Count of Bonus Name failed"));
            for (int i = 0; i < BonusName.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusName[i], BonusName[i].Text,
                    string.Format("Bonuses name {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusExpiryDate.Length, BonusExpiryDate.Count, "Count of Bonus Expiry Date failed"));
            for (int i = 0; i < BonusExpiryDate.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusExpiryDate[i], BonusExpiryDate[i].Text,
                    string.Format("Bonuses expiry date text {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesProgressBarRed, BonusesProgressBarRed.Count, "Count of red progress bars failed"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesProgressBarBlue, BonusesProgressBarBlue.Count, "Count of blue progress bars failed"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate.Length, OptionExpiryDate.Count, "Count of Option Expiry Date failed"));
            for (int i = 0; i < OptionExpiryDate.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate[i], OptionExpiryDate[i].Text,
                    string.Format("Option expiry date {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesResourceValue.Length, BonusesResourceValue.Count, "Count of Bonuses Resource Value failed"));
            for (int i = 0; i < BonusesResourceValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesResourceValue[i], BonusesResourceValue[i].Text,
                    string.Format("Bonuses value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesAbsValue.Length, BonusesAbsValue.Count, "Count of Bonuses Abs Value failed"));
            for (int i = 0; i < BonusesAbsValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesAbsValue[i], BonusesAbsValue[i].Text,
                    string.Format("Bonuses absolute value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceName.Length, TariffPlanResourceName.Count, "Count of Tariff Plan Resource Name failed"));
            for (int i = 0; i < TariffPlanResourceName.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceName[i], TariffPlanResourceName[i].Text,
                    string.Format("Bonuses absolute value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceValue.Length, TariffPlanResourceValue.Count, "Count of Tariff Plan Resource Value failed"));
            for (int i = 0; i < TariffPlanResourceValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceValue[i], TariffPlanResourceValue[i].Text,
                    string.Format("Tariff plan absolute value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceUnlim.Length, TariffPlanResourceUnlim.Count, "Count of Tariff Plan Гтдшьшеув Resource failed"));
            for (int i = 0; i < TariffPlanResourceUnlim.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanResourceUnlim[i], TariffPlanResourceUnlim[i].Text,
                    string.Format("Tariff plan unlimited {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanNote, TariffPlanNote.Text, "Tariff plan note failed"));
        }

        public void CheckLabelsPopupDeactivate(AssertsHelper _assertsAccumulator, string tariffPlan)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextPopupDeactivateTitle, tariffPlan), PopupTitle.Text, "Wrong text for title in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextPopupDeactivateInfo, tariffPlan), PopupInfo.Text, "Wrong text for info in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupDeactivateButton, PopupButton.Text, "Wrong text for button in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, PopupButtonCancel.Text, "Wrong text for button Cancel in popup"));
        }

        public string GetTariffPlanName()
        {
            return TestsHelper.GetWebText(TariffPlanName);
        }

        public bool isDeactivationSwitcherAvailable()
        {
            return TestsHelper.isWebEnabled(TariffDeactivationSwitcher);
        }

        public void ClickDeactivationSwitcher()
        {
            TestsHelper.ClickWeb(TariffDeactivationSwitcher);
        }

        public void CheckTariffPlanCard(AssertsHelper _assertsAccumulator)
        {
            for (int i = 0; i < ActiveTariffCard.Count; i++)
            {
                ClickTariffPlansDot(i);
                var title = ActiveTariffCard[i].FindElements(By.XPath(string.Format(ActiveTariffCardTitleXPath, i + 1)));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(title.Count == 1, "Tariff plan title failed or more than one"));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(title[0].Text.Length > 0, "Tariff plan title failed: is empty"));

                var characteristics = ActiveTariffCard[i].FindElements(By.XPath(string.Format(ActiveTariffCardCharacteristicXPath, i + 1)));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(characteristics.Count > 0, "Tariff plan characteristics failed: don't exist"));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(title[0].Text.Length > 0, "Tariff plan title failed: is empty"));

                var price = ActiveTariffCard[i].FindElements(By.XPath(string.Format(ActiveTariffCardPriceXPath, i + 1)));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(price.Count == 1, "Tariff plan price failed or more than one"));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(title[0].Text.Length > 0, "Tariff plan price failed: is empty"));
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(CheckFormatTariffPlanCard(title[0].Text), "Tariff plan price failed: format is wrong"));
            }
        }

        public void ClickTariffPlansDot(int index)
        {
            TestsHelper.ClickWeb(TariffCardCrousel[index]);
        }

        public void ClickTariffPlanCardButton(int index)
        {
            TestsHelper.ClickWeb(ActiveTariffCardButton[index]);
        }

        public int GetAvailableTariffPlansCnt()
        {
            return ActiveTariffCard.Count;
        }

        public string GetAvailableTariffPlanName(int index)
        {
            return TestsHelper.GetWebText(ActiveTariffCardTitle[index]);
        }

        public string GetAvailableTariffPlanPrice(int index)
        {
            return TestsHelper.GetWebText(ActiveTariffCardPrice[index]);
        }

        public void CheckLabelsPopupActivate(AssertsHelper _assertsAccumulator, string tariffPlan, string tariffPlanPrice)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextPopupActivateTitle, tariffPlan), PopupTitle.Text, "Wrong text for title in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextPopupActivateInfo, tariffPlan, tariffPlanPrice),
                PopupInfo.Text, "Wrong text for info in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupActivateButton, PopupButton.Text, "Wrong text for button in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, PopupButtonCancel.Text, "Wrong text for button Cancel in popup"));
        }

        private bool CheckFormatTariffPlanCard(string tariffPlanPrice)
        {
            int posCurrency = tariffPlanPrice.IndexOf("KM");
            if (posCurrency == 0)
            {
                return false;
            }
            if (!tariffPlanPrice.Substring(posCurrency, 2).Equals("KM"))
            {
                return false;
            }
            if (!tariffPlanPrice.Substring(posCurrency + 2, 1).Equals("/"))
            {
                return false;
            }
            if (tariffPlanPrice.Substring(posCurrency + 3, 1).Length <= 0)
            {
                return false;
            }
            if (tariffPlanPrice.Substring(posCurrency + 3, 1).Length <= 0)
            {
                return false;
            }
            if (tariffPlanPrice.Substring(0, posCurrency).Trim().Length <= 0)
            {
                return false;
            }
            decimal price;
            if (!Decimal.TryParse(tariffPlanPrice.Substring(0, posCurrency).Trim(), out price))
            {
                return false;
            }
            return true;
        }

        public void CheckLabelsGrace(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceTitle, GraceTitle.Text, "Wrong text for title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceInfo, GraceInfo.Text, "Wrong text for info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceButton, GraceButton.Text, "Wrong text for button on page"));
        }

        public void CheckLabelsPopupGrace(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupGraceTitle, PopupGraceTitle.Text, "Wrong text for title in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupGraceInfo, PopupGraceInfo.Text, "Wrong text for info in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceButton, PopupGraceButton.Text, "Wrong text for button in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextButtonCancel, PopupButtonCancel.Text, "Wrong text for button Cancel in popup"));
        }

        public void CheckLabelsPopupGraceWarning(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupGraceWarningTitle, PopupGraceWarningTitle.Text, "Wrong text for title in popup"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPopupGraceWarningInfo, PopupGraceWarningInfo.Text, "Wrong text for info in popup"));
        }

        public void ClickGraceActivateButton()
        {
            TestsHelper.ClickWeb(GraceButton);
        }

        public bool isPopupGraceActivateDisplayed()
        {
            return TestsHelper.isWebDisplayed(PopupGraceActivate);
        }

        public bool isPopupGraceWarningDisplayed()
        {
            return TestsHelper.isWebDisplayed(PopupGraceWarning);
        }

        public void ClickGraceWarningPopupClose()
        {
            TestsHelper.ClickWeb(PopupGraceWarningCloseButton);
        }

        public void CheckLabelsGraceActive3Days(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceTitle, GraceTitle.Text, "Wrong text for title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceInfo, GraceInfo.Text, "Wrong text for info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceButton, GraceButton.Text, "Wrong text for button on page"));
        }

        public bool isBannerShown()
        {
            return TestsHelper.isWebDisplayed(Banner);
        }

        public void CheckBannerResources(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextResourcesBannerHeader, BannerHeader.Text, "Wrong text for banner title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextResourcesBannerInfo, BannerInfo.Text, "Wrong text for banner info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextResourcesBannerButtons.Length, BannerButtons.Count, "Wrong number of buttons on banner"));
            for (int i = 0; i < BannerButtons.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextResourcesBannerButtons[i], BannerButtons[i].Text,
                    string.Format("Wrong text for banner button {0}", i)));
            }
        }

        public void CheckBannerAvailableTariffPlans(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAvailableTariffPlansBannerHeader, BannerHeader.Text, "Wrong text for banner title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAvailableTariffPlansBannerInfo, BannerInfo.Text, "Wrong text for banner info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAvailableTariffPlansBannerButtons.Length, BannerButtons.Count, "Wrong number of buttons on banner"));
            for (int i = 0; i < BannerButtons.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextAvailableTariffPlansBannerButtons[i], BannerButtons[i].Text,
                    string.Format("Wrong text for banner button {0}", i)));
            }
        }

        public void CheckBannerGraceActive3Days(AssertsHelper _assertsAccumulator)
        {
            var date = DateTime.Today.AddDays(2).Date.ToString().Substring(0, 10) + ".";
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(string.Format(TextGraceActiveBannerHeader, date), BannerHeader.Text,
                "Wrong text for banner title on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceActiveBannerInfo, BannerInfo.Text, "Wrong text for banner info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceActiveBannerInfoText, BannerInfoText.Text, "Wrong text for banner info text on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceActiveBannerButtons.Length, BannerButtons.Count, "Wrong number of buttons on banner"));
            for (int i = 0; i < BannerButtons.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextGraceActiveBannerButtons[i], BannerButtons[i].Text,
                    string.Format("Wrong text for banner button {0}", i)));
            }
        }
    }
}
