﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PostpaidMobileOverviewPage : PagesInner
    {
        public PostpaidMobileOverviewPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__tariff-name']")]
        private IWebElement ResourcesHeader;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__header']")]
        private IWebElement TariffOptionsHeader;

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__resource-name is-desktop']")]
        private IList<IWebElement> TariffResourceName;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__resource-info is-desktop']")]
        private IList<IWebElement> TariffOptionsNames;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses']//div[@class='bonuses__resource-info is-desktop']/div[@class='bonuses__resource-expiry-date']")]
        private IList<IWebElement> OptionExpiryDate;

        [FindsBy(How = How.XPath, Using = "//*[@class='progress-bar__filled progress-bar__filled--red']")]
        private IList<IWebElement> BonusesProgressBarRed;

        [FindsBy(How = How.XPath, Using = "//*[@class='progress-bar__filled progress-bar__filled--blue']")]
        private IList<IWebElement> BonusesProgressBarBlue; 

        [FindsBy(How = How.XPath, Using = "//*[@class='tariff-plan__resource-balance']")]
        private IList<IWebElement> TariffResourceValue;

        [FindsBy(How = How.XPath, Using = "//*[@class='bonuses__resource-values']")]
        private IList<IWebElement> TariffOptionsValue;

        [FindsBy(How = How.XPath, Using = "//*[@class='text-info']")]
        private IWebElement TariffPlanNote;


        /// to add tariff option section

        [FindsBy(How = How.XPath, Using = "//*[@class='page-mobile-postpaid-overview__subtitle']")]
        private IWebElement AddonsTableName;

        [FindsBy(How = How.XPath, Using = "//*[@class='adaptive-table__desktop-title']")]
        private IList<IWebElement> TableColumnName;  // 4

        [FindsBy(How = How.XPath, Using = "//*[@class='adaptive-table__desktop-cell']")]
        private IList<IWebElement> TableColumnValue;  // 4

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__overlay']")]
        private IWebElement Banner;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__header']")]
        private IWebElement BannerHeader;

        [FindsBy(How = How.XPath, Using = "//*[@class='banner__buttons']")]
        private IList<IWebElement> BannerButtons;

        private string TextBannerHeaderResources = "Paket: S";
        private int BannerButtonsCntResources = 1;

        [FindsBy(How = How.XPath, Using = "//*[@class='installments installments--desktop']")]
        private IWebElement Installments;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'desktop')]/div[@class='installments__title']")]
        private IWebElement InstallmentsTitle;

        private const string InstallmentsRowLeftXPath = "//*[contains(@class,'desktop')]/div/div[@class='installments__row'][{0}]/div[contains(@class,'left')]";
        private const string InstallmentsRowRightXPath = "//*[contains(@class,'desktop')]/div/div[@class='installments__row'][{0}]/div[contains(@class,'right')]";
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'desktop')]/div/div[@class='installments__row']")]
        private IList<IWebElement> InstallmentsRow;  // 2

        private const string InstallmentsColumnLeftXPath = "//*[contains(@class,'desktop')]/div/div[contains(@class,'column')][{0}]/div[contains(@class,'left')]";
        private const string InstallmentsColumnRightXPath = "//*[contains(@class,'desktop')]/div/div[contains(@class,'column')][{0}]/div[contains(@class,'right')]";
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'desktop')]/div/div[contains(@class,'column')]")]
        private IList<IWebElement> InstallmentsColumn;  // 2

        private string TextInstallmentsTitle = "Rate za uređaj";

        public bool isBannerShown()
        {
            return Helper.isWebExist(Banner);
        }

        public bool isInstallmentsShown()
        {
            return Helper.isWebExist(Installments);
        }

        public void CheckBanner(AssertsHelper _assertsAccumulator)
        {
            var bannerShown = isBannerShown();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(bannerShown, "Banner failed: is not shown"));
            if (bannerShown)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextBannerHeaderResources, BannerHeader.Text, "Banner header failed"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(BannerButtonsCntResources, BannerButtons.Count, "Count of Banner buttons failed"));
            }
        }

        public void CheckInstallments(AssertsHelper _assertsAccumulator)
        {
            var installmentsShown = isInstallmentsShown();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(installmentsShown, "Installments failed: is not shown"));
            if (installmentsShown)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextInstallmentsTitle, InstallmentsTitle.Text, "Installments header failed"));
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(BannerButtonsCntResources, BannerButtons.Count, "Count of Banner buttons failed"));
            }
        }

        public void CheckResources(AssertsHelper _assertsAccumulator, string serviceId)
        {
            var testData = new PostpaidMobileRersoucesTestData(serviceId);

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.ResourcesHeader, ResourcesHeader.Text, "Resources header failed"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffOpionsHeader, TariffOptionsHeader.Text, "Tariff opions header failed"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffResourceNames.Length, TariffResourceName.Count, "Count of resources names failed"));
            for (int i = 0; i < TariffResourceName.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffResourceNames[i], TariffResourceName[i].Text,
                    string.Format("Tariff resource name {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffOptionsNames.Length, TariffOptionsNames.Count, "Count of resources names failed"));
            for (int i = 0; i < TariffOptionsNames.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffOptionsNames[i], TariffOptionsNames[i].Text,
                    string.Format("Tariff option name {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate.Length, OptionExpiryDate.Count, "Count of Bonus Expiry Date failed"));
            for (int i = 0; i < OptionExpiryDate.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate[i], OptionExpiryDate[i].Text,
                    string.Format("Option expiry date text {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesProgressBarRed, BonusesProgressBarRed.Count, "Count of red progress bars failed"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.BonusesProgressBarBlue, BonusesProgressBarBlue.Count, "Count of blue progress bars failed"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate.Length, OptionExpiryDate.Count, "Count of Option Expiry Date failed"));
            for (int i = 0; i < OptionExpiryDate.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.OptionExpiryDate[i], OptionExpiryDate[i].Text,
                    string.Format("Option expiry date {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffResourceValue.Length, TariffResourceValue.Count, "Count of Tariff resource Value failed"));
            for (int i = 0; i < TariffResourceValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffResourceValue[i], TariffResourceValue[i].Text,
                    string.Format("Tariff resource value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffResourceValue.Length, TariffOptionsValue.Count, "Count of Tariff options Value failed"));
            for (int i = 0; i < TariffOptionsValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffOptionsValue[i], TariffOptionsValue[i].Text,
                    string.Format("Tariff option value {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.AddonsTableName, AddonsTableName.Text, "Addons table name failed"));

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TableColumnName.Length, TableColumnName.Count, "Count of addons table columns failed"));
            for (int i = 0; i < TableColumnName.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TableColumnName[i], TableColumnName[i].Text,
                    string.Format("Name of addons table column {0} failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TableColumnValue.Length, TableColumnValue.Count, "Count of Addons table values failed"));
            for (int i = 0; i < TableColumnValue.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TableColumnValue[i], TableColumnValue[i].Text,
                    string.Format("Addons value {0} in column failed", i)));
            }

            _assertsAccumulator.Accumulate(() => Assert.AreEqual(testData.TariffPlanNote, TariffPlanNote.Text, "Tariff plan note failed"));

            if (testData.IsBannerShown)
            {
                CheckBanner(_assertsAccumulator);
            }

            if (testData.IsInstallmentsShown)
            {
                CheckInstallments(_assertsAccumulator);
            }
        }
    }
}
