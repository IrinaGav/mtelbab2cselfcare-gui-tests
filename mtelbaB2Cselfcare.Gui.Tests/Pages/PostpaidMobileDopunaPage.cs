﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace mtelbaB2Cselfcare.Gui.Tests.Pages
{
    class PostpaidMobileDopunaPage : PagesInner
    {
        public PostpaidMobileDopunaPage(IWebDriver driver) : base(driver)
        { }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'main-layout__title')]")]
        private IWebElement PageHeader;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'tabs__controls-tab page')]")]
        private IList<IWebElement> PageTabs;  // 3

        [FindsBy(How = How.XPath, Using = "//*[@class='page-top-up__description']")]
        private IWebElement TabMbonInfo;

        [FindsBy(How = How.XPath, Using = "//*[@class='btn page-top-up__button-redirect']")]
        private IWebElement TabMbonButton;

        private string TextPageHeader = "ONLINE DOPUNA";
        private string[] TextPageTabs = { "PLATNA KARTICA", "ZADUŽIVANJEM RAČUNA", "M:BON DOPUNA" };
        private string TextTabMbonInfo = "m:bon - nov način dopune ili plaćanja računa. Posjetite sajt i saznajte više.";
        private string TextTabMbonButton = "Saznajte više";

        public void OpenTabMbon()
        {
            TestsHelper.ClickWeb(PageTabs[2]);
            isPageLoaded();
        }

        public void CheckLabels(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageHeader, PageHeader.Text, "Wrong text for title on page"));
            for (int i = 0; i < PageTabs.Count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextPageTabs[i], PageTabs[i].Text, "Wrong text for title on page"));
            }
        }

        public void CheckLabelsTabMbon(AssertsHelper _assertsAccumulator)
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTabMbonInfo, TabMbonInfo.Text, "Wrong text for info on page"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(TextTabMbonButton, TabMbonButton.Text, "Wrong text for button on page"));
        }

        public int GetTabsCount()
        {
            return PageTabs.Count;
        }

        public bool isTabEnabled(int index)
        {
            return TestsHelper.isWebEnabled(PageTabs[index]);
        }
    }
}
