﻿using mtelbaB2Cselfcare.Gui.Tests.Data;
using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using mtelbaB2Cselfcare.Gui.Tests.Pages;
using NUnit.Framework;

namespace mtelbaB2Cselfcare.Gui.Tests.TestStart
{
    [TestFixture]
    [Category("AddingCustomerAssets")]
    class AddingCustomerAssetsFirstLogin : TestSet
    {
        LoginPage loginPage;
        PregledPage pregledPage;

//      [Test, TestCaseSource(typeof(FeatureTestData), nameof(FeatureTestData.TestUsers))]
        public void FirstLogin_CustomerAssets_ShouldBeAdded(string user, string password, string firstOpenedPageUrl)
        {
            Assert.IsTrue(TestsHelper.OpenPage(driver, PageUrls.LoginUrl), string.Format("Login page is not opened, expected {0}, but is {1}", PageUrls.LoginUrl, driver.Url));
            loginPage = new LoginPage(driver);

            Assert.IsTrue(loginPage.loginByUser(user, password), string.Format("User {0} is not logged in. Test data is not added", user));

            _assertsAccumulator = new Helpers.AssertsHelper();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(driver.Url.Contains(firstOpenedPageUrl),
                string.Format("Pregled page is not opened, expected {0}, but is {1}", firstOpenedPageUrl, driver.Url)));

            pregledPage = new PregledPage(driver);
            pregledPage.isPageLoaded();

            var firstLoginPopupDisplayed = pregledPage.isFirstLoginPopupDisplayed();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(firstLoginPopupDisplayed, "FirstLoginPopup is not displayed"));

            if (firstLoginPopupDisplayed)
            {
                pregledPage.CloseFirstLoginPopup();
            }
            pregledPage.LogOut();
        }
    }
}
