﻿using mtelbaB2Cselfcare.Gui.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace mtelbaB2Cselfcare.Gui.Tests
{
    class TestSet
    {
        public IWebDriver driver;
        public AssertsHelper _assertsAccumulator;

        [OneTimeSetUp]
        public void startBrowserBeforeTestSuit()
        {
            driver = new ChromeDriver();

            TestsHelper.StartDriver(driver);
        }

        [OneTimeTearDown]
        public void closeBrowserAfterTestSuit()
        {
            driver.Quit();
        }
    }
}
